#ifndef MODULE_CUMULUS_H
#define MODULE_CUMULUS_H

#include "Arduino.h"
#include "../common/module.h"
#include "../common/telemetries.h"
// =====================================================================================================================
#define STATE_UNKNOWN   0
#define STATE_ON        1
#define STATE_OFF       2
// ---------------------------------------------------------------------------------------------------------------------
// /!\ id of the wiring, not pin id 
#define CUMULUS_WIRING_RELAY_PIN 0
// =====================================================================================================================
class ModuleCumulus: public Module{
private:
    int counter;
    static int state;    
    static int order;    
    static Wiring cumulus_relay;
public:
    ModuleCumulus(String aName, Wiring * wiring);
    virtual void setup_commands();
    virtual void setup_telemetry();
    virtual void setup_hardware();
    virtual void setup_states();

    static String process_command(DynamicJsonDocument * jsonArguments);

};
// =====================================================================================================================


#endif //MODULE_CUMULUS_H
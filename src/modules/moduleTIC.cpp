#include <ArduinoJson.h>          //https://github.com/bblanchon/ArduinoJson

#include "moduleTIC.h"
// ---------------------------------------------------------------------------------------------------------------------
// Members declared static in the header file are recalled here
Wiring ModuleTIC::tic_RX;
Wiring ModuleTIC::tic_TX;

// =====================================================================================================================
ModuleTIC::ModuleTIC(String aName, Wiring * wiring){
    Serial << "[ModuleTIC] [ModuleTIC] aName = " << aName << endl;
    this->setName(aName);
    this->setDescr("Téléinformation client compteur électrique");
    ModuleTIC::instance = this->getInstance();
    this->wiring = wiring;
}
// ---------------------------------------------------------------------------------------------------------------------
void ModuleTIC::process_telemetry(Telemetry  *aTelemetry){
    /* static */
    // callback for the telemetry1
    // should use external sensors to feed the measure values
    Serial << "[ModuleTIC] [process_telemetry] " << aTelemetry->getKey() << endl;


   // SoftwareSerial softSerial(ModuleTIC::tic_RX.pin, ModuleTIC::tic_TX.pin);
    SoftwareSerial softSerial(19, 3);
    TeleInfo teleinfo(&softSerial);
    //TeleInfo teleinfo(&Serial2);


    softSerial.begin(1200);
    teleinfo.begin();
    //teleinfo.setDebug(true);

    bool ok = false;
    while (not ok){
        teleinfo.process();
        if (teleinfo.available()){
            Serial.println("--- tele info available --- ");
            ok = true;
            //teleinfo.printAllToSerial();
            Serial.println("------ ");

            //const char *periodTarif = teleinfo.getStringVal("PTECZ");
            //Serial.print("Period Tarifaire = ");
            //periodTarif == NULL ? Serial.println("unknown") : Serial.println(periodTarif);

            const char *opTarif = teleinfo.getStringVal("OPTARIF");
            Serial.print("Option Tarifaire = ");
            opTarif == NULL ? Serial.println("unknown") : Serial.println(opTarif);

            long power = teleinfo.getLongVal("PAPP");
            Serial.print("Power = ");
            power < 0 ? Serial.println("unknown") : Serial.println(power);

            teleinfo.resetAvailable();
        }else{
        // Serial.println("-------------- tele info NOT available --- "); 
        }
    }

//    aTelemetry->getMeasure("T")->setValue(tempe);

    String jsonDataStr = aTelemetry->formatJsonFrame();
    char jsonData[500];
    jsonDataStr.toCharArray(jsonData, 500);
    Serial << "[ModuleTIC] [process_telemetry1] - jsonDataStr: " << jsonDataStr << endl;
    // TODO faire une methode moodule::publishdata
    Module * module = ModuleTIC::instance;
    ModuleTIC::instance->moduleList->publish_telemetry(aTelemetry, jsonData);
}
// ---------------------------------------------------------------------------------------------------------------------
void ModuleTIC::setup_telemetry(){
    Serial << "[ModuleTIC] [setup_telemetry]" << endl;
    Telemetry *telemetry1 = new Telemetry(this, "TIC", "Tic");
    telemetry1->register_measure("P", "Puissance", "W");

    telemetry1->getTicker()->attach(30, ModuleTIC::process_telemetry, telemetry1);
    this->telemetries.push_back(telemetry1);
}    
// ---------------------------------------------------------------------------------------------------------------------
void ModuleTIC::setup_hardware(){
    // this method setup the wiring of the different external sensors and actuators
    Serial << "[ModuleTIC] [setup_hardware] "  << endl;
    ModuleTIC::tic_RX = this->getWiringForPinId(TIC_WIRING_RX_PIN);
    ModuleTIC::tic_TX = this->getWiringForPinId(TIC_WIRING_TX_PIN); 
}
// ---------------------------------------------------------------------------------------------------------------------
void ModuleTIC::setup_states(){
}    
// ---------------------------------------------------------------------------------------------------------------------
void ModuleTIC::setup_commands(){
    Serial << "[ModuleTIC] [setup_commands]" << endl;
}
// =====================================================================================================================

#ifndef MODULE_TIC_H
#define MODULE_TIC_H

#include "Arduino.h"
#include <TeleInfo.h>
#include <SoftwareSerial.h>

#include "../common/module.h"
#include "../common/telemetries.h"
// =====================================================================================================================
// /!\ id of the wiring, not pin id 
#define TIC_WIRING_RX_PIN 0
#define TIC_WIRING_TX_PIN 1
// =====================================================================================================================
class ModuleTIC: public Module{
private:  
    static Wiring tic_RX;
    static Wiring tic_TX;

public:
    ModuleTIC(String aName, Wiring * wiring);
    virtual void setup_commands();
    virtual void setup_states();    
    virtual void setup_telemetry();
    virtual void setup_hardware();
    static void process_telemetry(Telemetry  *aTelemetry);

};
// =====================================================================================================================


#endif //MODULE_TIC_H
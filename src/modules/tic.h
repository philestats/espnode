#include <SoftwareSerial.h>
#define TELEINFO_RX 4
#define TELEINFO_TX 3 // not usefull, but the serial line needs 2 pins

#define TELEINFO_FRAME_START 0x02
#define TELEINFO_FRAME_END 0x03
#define TELEINFO_LINE_START 0x0A
#define TELEINFO_LINE_END 0x0D
#define MILLIS 1000
#define TIME_OUT_READ_LINE 30 * MILLIS

struct teleinfo_data
{
    boolean valid;          // if valid = false, reject the data
    String meter_id;        // num abonne
    unsigned long HC_index; // HC index
    unsigned long HP_index; // HP index
    int I_INST;             // instant Intensity
    int P_APP;              // Apparent power
};

// Serial line reading teleinfo
SoftwareSerial teleinfo(TELEINFO_RX, TELEINFO_TX);
// --------------------------------------------------------------------
char chksum(char *buff, uint8_t len)
{
    int i;
    char sum = 0;
    for (i = 1; i < (len - 2); i++)
        sum = sum + buff[i];
    sum = (sum & 0x3F) + 0x20;
    return (sum);
}
// --------------------------------------------------------------------
/*
 * read the line buff, search the differebt labels, extract and set the related values in the structure teleinfo_data
 */
void extractLine(char *buff, teleinfo_data *_data)
{
    String stringBuff = buff;
    // --------------------------------------------------------------------
    // Recherche du numero d'abonné
    int in = stringBuff.indexOf("ADCO");
    if (in == 1)
    {
        _data->meter_id = stringBuff.substring(in + 5, in + 17);
    }
    // --------------------------------------------------------------------
    // Recherche de l'index heures creuses
    in = stringBuff.indexOf("HCHC");
    if (in == 1)
    {
        _data->HC_index = atol(&buff[5]);
    }
    // --------------------------------------------------------------------
    // Recherche de l'index heures pleines
    in = stringBuff.indexOf("HCHP");
    if (in == 1)
    {
        _data->HP_index = atol(&buff[5]);
    }
    // --------------------------------------------------------------------
    // Recherche de Intensité instantanée
    in = stringBuff.indexOf("IINST");
    if (in == 1)
    {
        _data->I_INST = atoi(&buff[6]);
    }
    // --------------------------------------------------------------------
    //Recherche de Puissance apparente
    in = stringBuff.indexOf("PAPP");
    if (in == 1)
    {
        _data->P_APP = atoi(&buff[5]);
    }
}
// --------------------------------------------------------------------
int read_teleinfo(teleinfo_data * data)
{
    Serial.println("read_teleinfo debut");
    teleinfo.begin(1200);
    //teleinfo_data data;
    char charIn = 0;
    int nbCharLigne = 0;       //Nombres de caractères de la ligne
    char bufferLigne[21] = ""; //Caractères de la ligne
    int checkSum;              //Valeur du checksum
    unsigned long start_time = millis();
    unsigned long current_time;
    // Boucle d'attente du caractère de début de trame
Serial.println("attente TELEINFO_FRAME_START");
    while (charIn != TELEINFO_FRAME_START)
    {
        current_time = millis();
        /*Serial.print("read_teleinfo 4.1: en attente du caractere de debut. start_time = ");
        Serial.print(start_time);
        Serial.print("  current_time = ");
        Serial.println(current_time);*/
        // on "zappe" le 8ème bit, car d'après la doc EDF
        // la tramission se fait en 7 bits
        charIn = teleinfo.read() & 0x7F;
        //Serial.print(charIn);        
        //Serial.println("read_teleinfo 4.2");
        if (current_time - start_time >= TIME_OUT_READ_LINE)
        {
            Serial.println("read_teleinfo teleinfo time out");
            return -1;
        }
        yield();
    }
Serial.println("apres TELEINFO_FRAME_START");
Serial.println("attente TELEINFO_FRAME_END");

    // Boucle d'attente d'affichage des caractères reçus,
    // jusqu'à réception du caractère de fin de trame
    //On boucle tant que l'on n'a pas reçu le caractère de fin
    while (charIn != TELEINFO_FRAME_END)
    {
        //Serial.println("read_teleinfo 5.1: en attente du caractere de fin");
        //On lit tant qu'il y a des caractères à lire
        if (teleinfo.available())
        {
            //Stockage du caractère courant
            charIn = teleinfo.read() & 0x7F;

            //Si le caractère courant est un début de ligne
            if (charIn == TELEINFO_LINE_START)
            {
                //On réinitialise le nombre de caracteres
                nbCharLigne = 0;
                //Réinitialisation de l'array
                memset(bufferLigne, 0, sizeof(bufferLigne));
            }

            //Stockage du caractère de la ligne
            bufferLigne[nbCharLigne] = charIn;
            //Serial.print(charIn);
            //Si le caractère actuel est le caractère de fin de ligne
            if (charIn == TELEINFO_LINE_END)
            {
                //Récupération du checksum en fin de ligne
                checkSum = bufferLigne[nbCharLigne - 1];

                //Si le checksum est correct
                if (chksum(bufferLigne, nbCharLigne) == checkSum)
                {
//                    extractLine(bufferLigne, &data);
                    extractLine(bufferLigne, data);
                }
            }
            else
            {
                nbCharLigne++;
            }
        }
        yield();
    }
    teleinfo.end();
    data->valid = true;
    return 0;
}

bool getTeleinfo(char jsonPayload[100])
{
    Serial.println("[DEBUG] getTeleinfo debut");
    teleinfo_data data; 
    int r=read_teleinfo(&data);
    //StaticJsonBuffer<200> jsonBuffer;
//    JsonObject &root = jsonBuffer.createObject();
    char jsonChar[100];
    StaticJsonDocument<200> doc;
    JsonObject root = doc.as<JsonObject>();
    Serial.print("r="); Serial.println(r);
    if (data.valid){Serial.println("data.valid OUI");}else{Serial.println("data.valid NON");}
    if ((r == 0) && (data.valid))
    {
        char value[100];
        sprintf(value, "{\"id\":\"%u\",\"HC\":\"%lu\", \"HP\":\"%lu\", \"I\":\"%d\", \"P\":\"%d\"}", "device_id", data.HC_index, data.HP_index, data.I_INST, data.P_APP);
        sprintf(value, "{\"type\":\"TIC\",\"HC_index\":\"%lu\", \"HP_index\":\"%lu\", \"I_INST\":\"%d\", \"P_APP\":\"%d\"}", data.HC_index, data.HP_index, data.I_INST, data.P_APP);
        Serial.println(value);

        root["type"] = "TIC";
        root["HC_index"] = data.HC_index;
        root["HP_index"] = data.HP_index;
        root["I_INST"] = data.I_INST;
        root["P_APP"] = data.P_APP; 
        serializeJson(doc, jsonChar);
      //  root.printTo(jsonChar);
        Serial.println(jsonChar);
        memcpy(jsonPayload, jsonChar, 100);
        return true;
    }
    else
    {
        root["type"] = "TIC";
        root["msg"] = "Invalid data";
        Serial.println("[ERR] getTeleinfo: données invalides"); 
        serializeJson(doc, jsonChar);
       // root.printTo(jsonChar);
        Serial.println(jsonChar);
        memcpy(jsonPayload, jsonChar, 100);
        return false;
    }
}

#ifndef MODULE_PORTE_GARAGE_H
#define MODULE_PORTE_GARAGE_H

#include "Arduino.h"
#include "../common/module.h"
#include "../common/telemetries.h"

// =====================================================================================================================
#define STATE_UNKNOWN   0
#define STATE_OPEN      1
#define STATE_CLOSE     2
#define STATE_CLOSING   3
#define STATE_OPENING   4
// ---------------------------------------------------------------------------------------------------------------------
// /!\ id of the wiring, not pin id 
#define PORTE_WIRING_RELAY_PIN          0
#define PORTE_WIRING_OPEN_SENSOR_PIN    1
#define PORTE_WIRING_CLOSE_SENSOR_PIN   2
// =====================================================================================================================
class ModulePorteGarage: public Module{
private:
    static int current_state;    
    static int desired_state;
    static Wiring porte_relay;
    static Wiring porte_open_sensor;
    static Wiring porte_close_sensor;
public:
    ModulePorteGarage(String aName, Wiring * wiring);
    virtual void setup_commands();
    virtual void setup_telemetry();
    virtual void setup_hardware();
    virtual void setup_states();
    static String process_command(DynamicJsonDocument * jsonArguments);
    static void it_closed();
    static void it_opened(); // IRAM_ATTR ??
    static void set_state(int aState);
};
// =====================================================================================================================


#endif //MODULE_CUMULUS_H
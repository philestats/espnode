#include <ArduinoJson.h>          //https://github.com/bblanchon/ArduinoJson

#include "modulePorteGarage.h"
#define RETAINED_MSG true
// ---------------------------------------------------------------------------------------------------------------------
// Members declared static in the header file are recalled here
int ModulePorteGarage::current_state; 
int ModulePorteGarage::desired_state;
Wiring ModulePorteGarage::porte_relay;
Wiring ModulePorteGarage::porte_open_sensor;
Wiring ModulePorteGarage::porte_close_sensor;
// =====================================================================================================================
ModulePorteGarage::ModulePorteGarage(String aName, Wiring * wiring){
    // TODO: cette methode ne fait rien de particulier: passer dans la classe mere
    Serial << "[ModulePorteGarage] [ModulePorteGarage] aName = " << aName << endl;
    this->setName(aName);
    this->setDescr("Gestion de la porte de garage");
    ModulePorteGarage::instance = this->getInstance();
    ModulePorteGarage::set_state(STATE_UNKNOWN);
//    strcpy(ModulePorteGarage::consigne1, "UNKNOWN");
    this->wiring = wiring;
}
// ---------------------------------------------------------------------------------------------------------------------
String ModulePorteGarage::process_command(DynamicJsonDocument * jsonArguments){
    /* ---------- */
    /*   static   */
    /* ---------- */
    // This method process the response of the command: control
    Serial << "[ModulePorteGarage] [process_command] payload = " << "xx" << endl;

    // TODO se proteger si les arguments ne sont pas bien passés
    // on extrait l'uid
    if (jsonArguments->containsKey("uid")) {
        const char* uid = (*jsonArguments)["uid"];
        String strUid = String(uid);
        register_command_uid(strUid);
        Serial << "[ModulePorteGarage] [process_control] - Command: control uid: " << strUid << endl;
    }

    // et aussi la valeur de l'argument s'il y en a une
    Serial << "[ModulePorteGarage] [process_control] - 1" << endl;
    if (jsonArguments->containsKey("order")) {
        Serial << "[ModulePorteGarage] [process_control] - 2" << endl;
        const char* str_order_value = (*jsonArguments)["order"];
        if (strcmp(str_order_value, "OPEN") == 0){
            desired_state = STATE_OPEN;
        }else if (strcmp(str_order_value, "CLOSE") == 0){
            desired_state = STATE_CLOSE;
        }

        Serial << "[ModulePorteGarage] [process_control] - Command: control desired_state : " << desired_state << endl;
        Serial << "[ModulePorteGarage] [process_control] - Command: etat porte    : " << ModulePorteGarage::current_state << endl;
        // TODO lier l'argument comparé à ce qui est paramétré dans les arguments de la commande
        // ajouter un argument indiquant la porte
        
        // la porte est en cours de mouvement: on ne fait rien 
        if ((current_state ==  STATE_OPENING)|(ModulePorteGarage::current_state ==  STATE_CLOSING) ){
            Serial << "[ModulePorteGarage] [process_control] rejected: porte en mouvement " << ModulePorteGarage::current_state << endl;
            return COMMAND_REJECTED;
        }

        // la porte est deja dans l'etat consigne: on ne fait rien 
        if (current_state == desired_state){
            Serial << "[ModulePorteGarage] [process_control] rejected: deja dans l'etat " << desired_state << endl;
            return COMMAND_REJECTED;
        }

        instance->moduleList->generic_publish("toto", "[ModulePorteGarage] [process_control]");

        // la porte n'est pas déjà dans l'état consigne mais on ne sait pas comment elle est
        if (current_state == STATE_UNKNOWN){
            Serial << "[ModulePorteGarage] [process_control] mise en mouvement indéfini "  << endl;
            // l'etat reste STATE_UNKNOWN
            attachInterrupt(digitalPinToInterrupt(porte_open_sensor.pin), ModulePorteGarage::it_opened, FALLING);
            attachInterrupt(digitalPinToInterrupt(porte_close_sensor.pin), ModulePorteGarage::it_closed, FALLING);
            instance->send_pulse(porte_relay.pin, LOW, 100);
            return COMMAND_RUNNING;
        }

        // la porte est fermée et on veut l'ouvrir
        if (desired_state == STATE_OPEN){
            attachInterrupt(digitalPinToInterrupt(porte_open_sensor.pin), ModulePorteGarage::it_opened, FALLING);
            set_state(STATE_OPENING);
            instance->send_pulse(porte_relay.pin, LOW, 100);
            return COMMAND_RUNNING;
        }

        // la porte est ouverte et on veut la fermer
        if (desired_state == STATE_CLOSE){
            attachInterrupt(digitalPinToInterrupt(porte_close_sensor.pin), ModulePorteGarage::it_closed, FALLING);
            set_state(STATE_CLOSING);
            instance->send_pulse(porte_relay.pin, LOW, 100);
            return COMMAND_RUNNING;
        }
    }else if (jsonArguments->containsKey("request")){
        const char* str_request_value = (*jsonArguments)["request"];
        if (strcmp(str_request_value, "STATE") == 0){
            Serial << "request for current state: " << current_state << endl;
            return COMMAND_OK;
        }
    }else if (jsonArguments->containsKey("integer")) {
        int n = (*jsonArguments)["integer"];
        Serial << "[ModulePorteGarage] [process_control] integer : " << n << " " << endl;
        return COMMAND_OK;
    }else if (jsonArguments->containsKey("float")) {
        float a = (*jsonArguments)["float"];
        Serial << "[ModulePorteGarage] [process_control] float : " << a << endl;
        return COMMAND_OK;
    }else{
        return COMMAND_REJECTED;
    }
}
// ---------------------------------------------------------------------------------------------------------------------
void ModulePorteGarage::setup_commands(){
    Serial << "[ModulePorteGarage] [setup_commands]" << endl;
    Command *command1 = new Command(this, "control_porte", "Commande de contrôle de la porte 1");
    Command_arg order_open;
    order_open.setKey("order").setValue("OPEN").setDesc("Ouvrir la porte");

    Command_arg order_close;
    order_close.setKey("order").setValue("CLOSE").setDesc("Fermer la porte");

    Command_arg order_int;
    order_int.setKey("integer").setValue("$INT$").setDesc("envoyer un int");

    Command_arg order_float;
    order_float.setKey("float").setValue("$FLOAT$").setDesc("envoyer un float");

    Command_arg order_string;
    order_string.setKey("string").setValue("$INT$").setDesc("envoyer un string");

    Command_arg request_state;
    request_state.setKey("request").setValue("STATE").setDesc("Interroger");
    //wiring_Cumulus[0].setPin(CUMULUS_RELAY_PIN).setDesc("Commande relai").setId(CUMULUS_WIRING_RELAY_PIN);

    command1->add_argument(order_open)->add_argument(order_close)->add_argument(request_state);
    command1->add_argument(order_int)->add_argument(order_float)->add_argument(order_string);
    command1->execute = &ModulePorteGarage::process_command;
    this->commands.push_back(command1);
}
// ---------------------------------------------------------------------------------------------------------------------
void ModulePorteGarage::setup_telemetry(){
}
// ---------------------------------------------------------------------------------------------------------------------
void ModulePorteGarage::setup_states(){
    State * state_porte = new State(this, "pos1", "Position Porte");
    // TODO lire les capteurs de position
    state_porte->setValue("UNKNOWN");
    this->states.push_back(state_porte);
}
// ---------------------------------------------------------------------------------------------------------------------
void ModulePorteGarage::it_closed() {
    /* ---------- */
    /*   static   */
    /* ---------- */
    detachInterrupt(porte_close_sensor.pin);
    if (desired_state == STATE_CLOSE){
        Serial << "[ModulePorteGarage] [it_closed] consigne = CLOSE: porte fermée: terminé"  << endl;
        set_state(STATE_CLOSE);
        instance->enqueue_feedback(Module::get_command_uid(), COMMAND_EXECUTED);
    }else if (desired_state == STATE_OPEN){
        Serial << "[ModulePorteGarage] [it_closed] consigne = OPEN: porte fermée: renvoi impulsion"  << endl;
        instance->send_pulse(porte_relay.pin, LOW, 100);
        set_state(STATE_OPENING);
    }
}
// ---------------------------------------------------------------------------------------------------------------------
void ModulePorteGarage::set_state(int aState){
    /* ---------- */
    /*   static   */
    /* ---------- */
    Serial << "[ModulePorteGarage] [set_state] current_state = " << aState << endl;
    current_state = aState;
}
// ---------------------------------------------------------------------------------------------------------------------
void ModulePorteGarage::it_opened() {
    /* ---------- */
    /*   static   */
    /* ---------- */
    detachInterrupt(porte_open_sensor.pin);
    if (desired_state == STATE_OPEN){
        Serial << "[ModulePorteGarage] [it_opened] consigne = OPEN: porte ouverte: terminé"  << endl;
        set_state(STATE_OPEN);
        instance->enqueue_feedback(Module::get_command_uid(), COMMAND_EXECUTED);
    }else if (desired_state == STATE_CLOSE){
        Serial << "[ModulePorteGarage] [it_opened] consigne = CLOSE: porte ouverte: renvoi impulsion"  << endl;
        instance->send_pulse(porte_relay.pin, LOW, 100);
        set_state(STATE_CLOSING);
    }
}
// ---------------------------------------------------------------------------------------------------------------------
void ModulePorteGarage::setup_hardware(){
    // this method setup the wiring of the different external sensors and actuators
    Serial << "[ModulePorteGarage] [setup_hardware] "  << endl;
    porte_relay = this->getWiringForPinId(PORTE_WIRING_RELAY_PIN);
    porte_open_sensor = this->getWiringForPinId(PORTE_WIRING_OPEN_SENSOR_PIN);
    porte_close_sensor = this->getWiringForPinId(PORTE_WIRING_CLOSE_SENSOR_PIN);
    pinMode(porte_relay.pin, OUTPUT);
    // interruptions
    pinMode(porte_close_sensor.pin, INPUT_PULLUP);
    pinMode(porte_open_sensor.pin, INPUT_PULLUP);
    // init relay
    digitalWrite(porte_relay.pin, HIGH);
}
// =====================================================================================================================

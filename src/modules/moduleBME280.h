#ifndef MODULE_BME280_H
#define MODULE_BME280_H

#include "Arduino.h"
#include "../common/module.h"
#include "../common/telemetries.h"
// =====================================================================================================================
// /!\ id of the wiring, not pin id 
#define BME280_WIRING_SCL_PIN 0
#define BME280_WIRING_SDA_PIN 1
// =====================================================================================================================
class ModuleBME280: public Module{
private:  
    static Wiring bme280_I2C_SCL;
    static Wiring bme280_I2C_SDA;
    static Wiring bme280_I2C_VIN;
    static Wiring bme280_I2C_GND;

public:
    ModuleBME280(String aName, Wiring * wiring);
    virtual void setup_commands();
    virtual void setup_states();    
    virtual void setup_telemetry();
    virtual void setup_hardware();
    static void process_telemetry(Telemetry  *aTelemetry);

};
// =====================================================================================================================


#endif //MODULE_BME280_H
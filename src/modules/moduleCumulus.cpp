#include <ArduinoJson.h>          //https://github.com/bblanchon/ArduinoJson

#include "moduleCumulus.h"

#define RETAINED_MSG true
// ---------------------------------------------------------------------------------------------------------------------
// Members declared static in the header file are recalled here
Wiring ModuleCumulus::cumulus_relay;
int ModuleCumulus::state; 
int ModuleCumulus::order;
// =====================================================================================================================
ModuleCumulus::ModuleCumulus(String aName, Wiring * wiring){
    // TODO: cette methode ne fait rien de particulier: passer dans la classe mere
    Serial << "[ModuleCumulus] [ModuleCumulus] aName = " << aName << endl;
    this->setName(aName);
    this->setDescr("Gestion du chauffe-eau");
    // this->state = "off";
    ModuleCumulus::instance = this->getInstance();
    counter = 0;
    this->wiring = wiring;
}
// ---------------------------------------------------------------------------------------------------------------------
String ModuleCumulus::process_command(DynamicJsonDocument * jsonArguments){
    // This method process the response of the command: control
    Serial << "[ModuleCumulus] [process_control] payload = " << "xx" << endl;
    DynamicJsonDocument doc = *jsonArguments;

    // on extrait l'uid
    const char* uid = doc["uid"];
    Serial << "[ModuleCumulus] [process_control] - Command: control uid: " << uid << endl;
    // et aussi la valeur de l'argument s'il y en a une
    const char* str_order = doc["order"];

    if (strcmp(str_order, "ON") == 0){
        order = STATE_ON;
    }else if (strcmp(str_order, "OFF") == 0){
        order = STATE_OFF;
    }
    Serial << "[ModuleCumulus] [process_control] - Command: control order : " << order << endl;

    if (strcmp(str_order, "ON") == 0){
// ModuleCumulus::instance->state = "on";
        Serial << "[ModuleCumulus] [process_control] - Setting CUMULUS_RELAY TO ON" << endl;
        digitalWrite(cumulus_relay.pin, LOW);  
        return COMMAND_OK;
    }else if (strcmp(str_order, "OFF")== 0){
        Serial << "[ModuleCumulus] [process_control] - Setting CUMULUS_RELAY TO OFF" << endl;
// ModuleCumulus::instance->state = "off";
        digitalWrite(cumulus_relay.pin, HIGH);  
        return COMMAND_OK;
    }else if (strcmp(str_order, "STATUS")== 0){
//        Serial << "[ModuleCumulus] [processCommand] - state: " << this->state << endl;
        // TODO faire une methode abstraite qui formate les responses
        char state[20];
 //       this->state.toCharArray(state, sizeof(state));
        StaticJsonDocument<400> doc;
        JsonObject root = doc.to<JsonObject>();
        root["uid"] = uid;
        root["response"] = state;
        char resp[JSON_MSG_SZ];
        serializeJson(doc, resp);
        Serial << "[ModuleCumulus] [processCommand] - resp: " << resp << endl;

//        this->pubSub->publish(topics.response, resp, RETAINED_MSG);
        return COMMAND_OK;
    }else{
        return COMMAND_UNKNOWN_ARG;
    }
    free(jsonArguments);
}
// ---------------------------------------------------------------------------------------------------------------------
void ModuleCumulus::setup_commands(){
    Serial << "[ModuleCumulus] [setup_commands]" << endl;
    Command *command1 = new Command(this, "control", "Commande de contrôle du cumulus");
    Command_arg order_ON;
    order_ON.setKey("order").setValue("ON").setDesc("Allumer le cumulus");
    Command_arg order_OFF;
    order_OFF.setKey("order").setValue("OFF").setDesc("Eteindre le cumulus");
    Command_arg order_STATE;
    order_STATE.setKey("order").setValue("STATE").setDesc("Etat du cumulus");
    command1->add_argument(order_ON)->add_argument(order_OFF)->add_argument(order_STATE);
    command1->execute = &ModuleCumulus::process_command;
    this->commands.push_back(command1);
}
// ---------------------------------------------------------------------------------------------------------------------
void ModuleCumulus::setup_telemetry(){
}    
// ---------------------------------------------------------------------------------------------------------------------
void ModuleCumulus::setup_states(){
    State * state1 = new State(this, "cum", "Cumulus en marche");
    this->states.push_back(state1);
}
// ---------------------------------------------------------------------------------------------------------------------
void ModuleCumulus::setup_hardware(){
    // this method setup the wiring of the different external sensors and actuators
    Serial << "[ModuleCumulus] [setup_hardware] "  << endl;
    cumulus_relay = this->getWiringForPinId(CUMULUS_WIRING_RELAY_PIN);
    pinMode(cumulus_relay.pin, OUTPUT);
    digitalWrite(cumulus_relay.pin, HIGH);
}
// =====================================================================================================================

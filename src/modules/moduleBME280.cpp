#include <ArduinoJson.h>          //https://github.com/bblanchon/ArduinoJson

#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>
#include "moduleBME280.h"
#define SEALEVELPRESSURE_HPA (1013.25)
// ---------------------------------------------------------------------------------------------------------------------
// Members declared static in the header file are recalled here
Wiring ModuleBME280::bme280_I2C_SCL;
Wiring ModuleBME280::bme280_I2C_SDA;
Wiring ModuleBME280::bme280_I2C_VIN;
Wiring ModuleBME280::bme280_I2C_GND;
// =====================================================================================================================
ModuleBME280::ModuleBME280(String aName, Wiring * wiring){
    Serial << "[ModuleBME280] [moduleBME280] aName = " << aName << endl;
    this->setName(aName);
    this->setDescr("Capteur de température BME280");
    ModuleBME280::instance = this->getInstance();
    this->wiring = wiring;
    // no need to initialize pins as we use standard I2C pins 21 and 22
}
// ---------------------------------------------------------------------------------------------------------------------
void ModuleBME280::process_telemetry(Telemetry  *aTelemetry){
    /* static */
    // callback for the telemetry1
    // should use external sensors to feed the measure values
    Serial << "[ModuleBME280] [process_telemetry] " << aTelemetry->getKey() << endl;
    Adafruit_BME280 * bme = new Adafruit_BME280(); // I2C
    // default settings

    unsigned status;
    
    // default settings
    status = bme->begin();  
    // You can also pass in a Wire library object like &Wire2
    // status = bme.begin(0x76, &Wire2)
    if (!status) {
        Serial << "[ModuleBME280] [process_telemetry] Could not find a valid BME280 sensor, check wiring, address, sensor ID!" << endl;
        Serial << "[ModuleBME280] [process_telemetry] SensorID was: 0x" << bme->sensorID() << endl;
    }
    float tempe = bme->readTemperature();
    float pressure = bme->readPressure() / 100.0F;
    float humidity = bme->readHumidity();
    free(bme);


    aTelemetry->getMeasure("T")->setValue(tempe);
    aTelemetry->getMeasure("P")->setValue(pressure);
    aTelemetry->getMeasure("H")->setValue(humidity);
    String jsonDataStr = aTelemetry->formatJsonFrame();
    char jsonData[500];
    jsonDataStr.toCharArray(jsonData, 500);
    Serial << "[ModuleCumulus] [process_telemetry1] - jsonDataStr: " << jsonDataStr << endl;
    // TODO faire une methode moodule::publishdata
    Module * module = ModuleBME280::instance;
    ModuleBME280::instance->moduleList->publish_telemetry(aTelemetry, jsonData);
}
// ---------------------------------------------------------------------------------------------------------------------
void ModuleBME280::setup_telemetry(){
    Serial << "[ModuleCumulus] [setup_telemetry]" << endl;
    Telemetry *telemetry1 = new Telemetry(this, "TPH", "Temperature, pression, humidité capteur BME280");
    telemetry1->register_measure("T", "Temperature", "deg C");
    telemetry1->register_measure("H", "Humidity", "%");
    telemetry1->register_measure("P", "Pressure", "hPa");
    telemetry1->getTicker()->attach(10, ModuleBME280::process_telemetry, telemetry1);
    this->telemetries.push_back(telemetry1);
}    
// ---------------------------------------------------------------------------------------------------------------------
void ModuleBME280::setup_hardware(){
    // this method setup the wiring of the different external sensors and actuators
    Serial << "[ModuleBME280] [setup_hardware] "  << endl;
}
// ---------------------------------------------------------------------------------------------------------------------
void ModuleBME280::setup_states(){
}    
// ---------------------------------------------------------------------------------------------------------------------
void ModuleBME280::setup_commands(){
    Serial << "[ModuleBME280] [setup_commands]" << endl;
}
// =====================================================================================================================

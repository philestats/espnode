#ifndef LINK_H
#define LINK_H

#include "Arduino.h"
#include <Streaming.h>
#include <PubSubClient.h>
#include <Vector.h>
#include "BluetoothSerial.h"

#include "hermes_protocol.h"
#include "device.h"
#include "message.h"
 
#define QUEUE_SIZE 10
class Device;

// =====================================================================================================================
class Link {
protected:
    Topics * topics; 
public:
    Message message_queue_storage_array[QUEUE_SIZE];
    Vector<Message> message_queue;
    static Device * device; // this static member shall be redeclared in the C++ file  http://www.cs.technion.ac.il/users/yechiel/c++-faq/link-errs-static-data-mems.html
    // static Link * instance; 
    // char queue_size = 0;
    // char queue_topic[64];
    // char queue_message[1024];
    Link();
    void setDevice(Device * aDevice);
    virtual void enqueue(Message aMessage)=0;
    virtual void publish(char * aChannel, char * aMessage)=0;
    virtual void display()=0;
    virtual void setTopics(Topics * aTopics)=0;
    virtual void loop()=0;
};    
// =====================================================================================================================
class MqttLink: public Link{
private:
    WiFiClient * espClient;
    PubSubClient * pubsub;
public:
    MqttLink();
    void subscribe_topics();
    static void mqttCallback(char* topic, byte* payload, unsigned int length);
    void publish(char * aChannel, char * aPayload);
    virtual void enqueue(Message aMessage);
    void display();
    void setTopics(Topics * aTopics);
    void loop();
};
// =====================================================================================================================
class LoraLink: public Link{
public:
    LoraLink(/* args */);
    void publish(char * aChannel, char * aPayload);
    void display();
};
// =====================================================================================================================
class BluetoothLink: public Link{
private:
    BluetoothSerial * SerialBT;
public:
    BluetoothLink(/* args */);
    void publish(char * aChannel, char * aPayload);
    void display(); 
    virtual void enqueue(Message aMessage);
    virtual void setTopics(Topics * aTopics);
    virtual void loop();
};
// =====================================================================================================================
#endif
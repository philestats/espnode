#ifndef HERMES_PROTOCOL
#define HERMES_PROTOCOL

#include <WiFiClient.h>
#include <PubSubClient.h>



#ifdef ESP32
    #define ESP_reboot()        (ESP.restart())
    #define HW_TYPE             "ESP32"
#else
    #define ESP_reboot()        (ESP.reset())
    #define HW_TYPE             "OTHER"
#endif

#define SOFTWARE_NAME           "esp-node"
#define SOFTWARE_VERSION        "0.1.0"

#define MQTT_TOPIC_BASE_SENSOR      "sensors"
#define MQTT_TOPIC_BASE_NODE        "nodes"
#define MQTT_TOPIC_BASE_SYSTEM      "system"

#define MQTT_TOPIC_HELLO            "hello"
#define MQTT_TOPIC_ERROR            "error"
#define MQTT_TOPIC_COMMAND          "command"
#define MQTT_TOPIC_RESPONSE         "response"
#define MQTT_TOPIC_BROADCAST        "broadcast"
#define MQTT_TOPIC_DATA             "data"
#define MQTT_TOPIC_FEEDBACK         "feedback"

// LOOP_MODE_RESET: at the end of loop, the esp waits SLEEP_TIME_SECONDS and reboots
#define LOOP_MODE_RESET             1
// LOOP_MODE_SLEEP: at the end of loop, the esp goes to sleep
#define LOOP_MODE_SLEEP             2
// LOOP_INFINITE: the loop function loops :-)
#define LOOP_INFINITE               3
// =====================================================================================================================
#define COMMAND_NYIMPLD             "CMD_NOT_YET_IMPLEMENTED"
#define COMMAND_OK                  "CMD_OK"
#define COMMAND_KO                  "CMD_KO"
#define COMMAND_NOT_FOUND           "CMD_NOT_FOUND"
#define COMMAND_MODULE_NOT_FOUND    "CMD_MODULE_NOT_FOUND"
#define COMMAND_UNKNOWN_ARG         "CMD_UNKNOWN_ARG"
#define COMMAND_ARGS_OK             "COMMAND_ARGS_OK"
#define COMMAND_REJECTED            "COMMAND_REJECTED"
#define COMMAND_RUNNING             "CMD_RUNNING"
#define COMMAND_EXECUTED            "CMD_EXECUTED"
// =====================================================================================================================
typedef struct {
    char hello[64];
    char command[64];
    char response[64];
    char feedback[64];
    char telemetry[64];
    char debug[64];
    char error[64];
    // char broadcast[64];
} Topics;
// =====================================================================================================================
#define JSON_MSG_SZ                 500

String get_printable_mac_address();

#endif
#ifndef MODULE_H
#define MODULE_H

#include "Arduino.h"
#include <Streaming.h>
#include <Ticker.h>
#include "hermes_protocol.h"
#include "device.h"
#include "telemetries.h"
#include "commands.h"
#include "states.h"
#include "message.h"
#include "wiring.h"

#define MODULES_MAX       10
#define COMMANDS_MAX    10
#define TELEMETRIES_MAX 10
#define STATES_MAX 10

#define TOPIC_DATA      data
#define TOPIC_FEEDBACK  feedback
// =====================================================================================================================
class Device;
class ModuleList;
// =====================================================================================================================
// typedef struct {
//     String desc;
//     int pin;
// } wiring;
// https://stackoverflow.com/questions/6181715/convenient-c-struct-initialisation
// =====================================================================================================================
class Module {
private:
    String name;
    String descr;
    // array storing the pointers to commands objects
    Command* commands_storage_array[COMMANDS_MAX];
    Telemetry* telemetries_storage_array[TELEMETRIES_MAX];
    State* states_storage_array[STATES_MAX];
    // the command currently running
    static String current_command_uid;
protected:    
    Wiring * wiring;
    Wiring getWiringForPinId(int wiringId);
public:
    Module();
    ModuleList* moduleList;
    // Module(String aName);
    static void register_command_uid(String uid);
    static void un_register_command_uid(const char* uid);
    static String get_command_uid();
    void setModuleList(ModuleList * aModuleList);
    void setName(String aName);
    String getName();
    void setDescr(String aDescr);
    String getDescr();
    void print();
    // returns a pointer to a command stored in commands_storage_array with key=command_key
    Command * findCommand(String command_key);
    virtual void setup_hardware() = 0;  // pure virtual method
    virtual void setup_commands() = 0;  // pure virtual method
    virtual void setup_telemetry() = 0;  // pure virtual method
    virtual void setup_states() = 0;  // pure virtual method
    virtual String processCommand(Command * aCommand, DynamicJsonDocument * jsonDocArgs);
    virtual void init();
    virtual Module * getInstance();
    Vector<Command*> commands;
    Vector<Telemetry*> telemetries;
    Vector<State*> states;
    void enqueue(Message aMessage);
    void enqueue_feedback(String command_uid, const char* command_result);
    void publish(Message aMessage);
    void send_pulse(int pin, int direction, int duration);
    void setTopics(Topics * aTopics);
    Topics topics;
protected:
    static Module *instance;

};
// =====================================================================================================================
class ModuleList{
private:
    Module* module_storage_array[MODULES_MAX];
    Vector<Module*> modules;
    Module * findModule(String module_name);
public:
    ModuleList();
    Topics * topics; 
    Device * device;
    void add(Module *aModule);
    void print();
    void setDevice(Device * aDevice);
    void generic_publish(char * aChannel, char * aMessage);
    void publish_telemetry(Telemetry * aTelemetry, String aMessage);
    String getSpecifications(String uid);
    String forwardCommand(String module_name, String command, String arguments);
    void setTopics(Topics * aTopics);
};
// =====================================================================================================================

#endif //MODULES_H
#ifndef TELEMETRIES_H
#define TELEMETRIES_H

#include <Arduino.h>
#include <Vector.h>
#include <Ticker.h>
#include "hermes_protocol.h"


// max nb of measurements in a telemetry
#define MEASURES_MAX   5

class Module;
// =====================================================================================================================
class Measure{
    private:
        String key;
        String desc;
        String unit;
        float value;
    public:
        Measure();
        Measure(String aKey, String aDesc, String aUnit);
        void setValue(float aValue);
        float getValue();
        String getKey();
        String getDesc();
};
// =====================================================================================================================
class Telemetry{
/* A telemetry object has several measurements
*/
    private:
        int nb_measures;
        int interval;
        String key;
        String desc;
        //Command_arg args[1];
        Measure * measures_storage_array[MEASURES_MAX];
        // the owner is the module
        Ticker * ticker;
        void add_measure(Measure * aMeasure);
    public:
        Telemetry(Module * aModule, String aTelemetryKey, String aTelemetry_desc);
        Module * owner;
        void register_measure(String aMeasureKey, String aDesc, String aUnit);
        Measure * getMeasure(String aMeasureKey);
        void print();
        Vector<Measure*> measures;
        String toJson();
        String getKey();
        String getDesc();
        Ticker * getTicker();
        String formatJsonFrame();
};
// =====================================================================================================================
#endif
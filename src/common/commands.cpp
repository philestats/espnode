#include "commands.h"
// https://github.com/janelia-arduino/Streaming
// https://github.com/janelia-arduino/Vector/
#include <Vector.h>
#include <Streaming.h>

#include "module.h"

// ---------------------------------------------------------------------------------------------------------------------
Command::Command(Module * aModule, String aCommand_name, String aCommand_desc){
    //Serial << "[Command] Constructor " << aCommand_name << endl;
    this->key = aCommand_name;
    this->desc = aCommand_desc;
    this->owner = aModule;
    this->arguments.setStorage(this->args_storage_array);
}
// ---------------------------------------------------------------------------------------------------------------------
Command * Command::add_argument(Command_arg arg){
    this->arguments.push_back(arg);
    return this;
}
// ---------------------------------------------------------------------------------------------------------------------
String Command::getKey(){
    return this->key;
}
// ---------------------------------------------------------------------------------------------------------------------
String Command::getDesc(){
    return this->desc;
}
// ---------------------------------------------------------------------------------------------------------------------
void Command::print(){
    Serial << "---"<< endl;
    Serial << this->key  << " " << this->desc << endl;
    Serial << "    Arguments: " << endl;
    for (int i=0; i<this->arguments.size(); i++){
        Serial << "       Clé:   " << this->arguments[i].key << "   ( " << this->arguments[i].desc << ")" << endl;
    }
}
// ---------------------------------------------------------------------------------------------------------------------
String Command::toJson(){
    Serial << "---"<< endl;
    Serial << this->key  << " " << this->desc << endl;
    Serial << "    Arguments: " << endl;
    String commandDump = "{ \"name\":\"" +  this->key + "\", \"args\": [";
    bool add_comma = false;
    for (int i=0; i<this->arguments.size(); i++){
        Serial << "       Clé:   " << this->arguments[i].key << "   ( " << this->arguments[i].desc << ")" << endl;
        if (add_comma){ commandDump = commandDump + ","; }else{ add_comma = true; }
        commandDump = commandDump + "{\"key\": \"" +  this->arguments[i].key + "\", \"desc\": \"" + this->arguments[i].desc + "\"}";   
    }
    commandDump = commandDump + "]}";
    return commandDump;
}
// ---------------------------------------------------------------------------------------------------------------------
String Command::executeCmd(DynamicJsonDocument * jsonDocArgs){
    Serial << "[Command] [executeCmd] - execute command: " << this->key << endl;
    // TODO: la commande a un callback qui est un pointeur sur une methode de Module
    // l'exécution de la commande est demandée au module
    return this->owner->processCommand(this, jsonDocArgs);
}
// ---------------------------------------------------------------------------------------------------------------------
String Command::checkArguments(String arguments, DynamicJsonDocument * jsonArguments){
    Serial << "[Command] [checkArguments] - args: " << arguments << endl;
    // Deserialize the arguments passed as String into a DynamicJsonDocument
    // if all checks are ok, the DynamicJsonDocument jsonArguments is returned
    deserializeJson(*jsonArguments, arguments);
    JsonObject rootJsonArguments = jsonArguments->as<JsonObject>();

    Serial << "------------------------------------ " << endl;
    bool arg_ok = false;
    // https://arduinojson.org/v6/api/jsonobject/begin_end/
    // https://arduinojson.org/v6/api/dynamicjsondocument/
    // on boucle sur les arguments passés

    for (JsonObject::iterator passedArgIt=rootJsonArguments.begin(); passedArgIt!=rootJsonArguments.end(); ++passedArgIt) {
        Serial << "key: " << passedArgIt->key().c_str() << " - value: " << passedArgIt->value().as<char*>() << endl;
        if (passedArgIt->key() ==  "uid"){
            Serial << "found uid" << endl;
            continue;
        }
        // on boucle sur les arguments autorisés
        for (Command_arg definedArg : this->arguments){
            String value = passedArgIt->value().as<String>();
            Serial << "  -" << definedArg.key << " - " << definedArg.value << " - " << definedArg.desc << endl;
            // clé et value de l'argument passé correspondent à la définition d'un argument paramétré
            if ((definedArg.key == String(passedArgIt->key().c_str())) && (definedArg.value == value) ){
                arg_ok = true;
                break;
            }
            if ((definedArg.key == String(passedArgIt->key().c_str())) && (definedArg.value == "$INT$") ){
                // todo check que la value passée est un int
                if (passedArgIt->value().is<int>()){
                    int intVal = passedArgIt->value().as<int>();
                    Serial << "  [break] int passedArgIt->value().as<int>():   " << intVal << endl;
                    arg_ok = true;
                }
                break;
            }
            if ((definedArg.key == String(passedArgIt->key().c_str())) && (definedArg.value == "$FLOAT$") ){
                // todo check que la value passée est un float
                if (passedArgIt->value().is<float>()){
                    float floatVal = passedArgIt->value().as<float>();
                    Serial << "  [break] float passedArgIt->value().as<float>():   " << floatVal << endl;
                    arg_ok = true;
                }
                break;
            }
            if ((definedArg.key == String(passedArgIt->key().c_str())) && (definedArg.value == "$STRING$") ){
                // todo check que la value passée est un string
                if (passedArgIt->value().is<char*>()){
                    const char* charVal = passedArgIt->value().as<char*>();
                    Serial << "  [break] char passedArgIt->value().as<char*>():   " << charVal << endl;
                    arg_ok = true;
                }
                break;
            }
        }

        if (! arg_ok){
            Serial << "[NOT FOUND] argument " << String(passedArgIt->key().c_str()) << " - value: " << passedArgIt->value().as<char*>()  << endl;
            Serial << "------------------------------------ NOK" << endl;
            return COMMAND_UNKNOWN_ARG;
        }else{
            Serial << "[FOUND] argument " << String(passedArgIt->key().c_str()) << " - value: " << passedArgIt->value().as<char*>() << endl;
  
        }
    }
    Serial << "------------------------------------ OK" << endl;
    return COMMAND_ARGS_OK;
}
// =====================================================================================================================
Commands_list::Commands_list(){
    this->nb_cmds = 0;
    Serial << "[Commands_list] Constructor" << endl;
}
// ---------------------------------------------------------------------------------------------------------------------
Commands_list Commands_list::add_command(Command cmd){
    this->commands[nb_cmds] = cmd;
    this->nb_cmds++;
}
// =====================================================================================================================
void Commands_list::print(){
    Serial << "[Commands_list] nb commands: " << this->nb_cmds << endl;
}
// =====================================================================================================================

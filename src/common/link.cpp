#include "Arduino.h"
#include <Streaming.h>
#include "WiFi.h"
#include "BluetoothSerial.h"

// #include <BLEDevice.h>
// #include <BLEServer.h>
// #include <BLEUtils.h>
// #include <BLE2902.h>
// #include "DHT.h"

#include "link.h"
class Device;

Device * Link::device;   // here the static member is redeclared 
// =====================================================================================================================
Link::Link(){
    Serial << "[Link] [Link] " << endl;
    this->message_queue.setStorage(this->message_queue_storage_array);
}
// ---------------------------------------------------------------------------------------------------------------------
void Link::setDevice(Device * aDevice){
    Link::device = aDevice;
}
// ---------------------------------------------------------------------------------------------------------------------
// void Link::enqueue(Message aMessage){
//     Serial << "[Link] [enqueue] " << aMessage.channel << " - " << aMessage.payload << endl;    
//     this->message_queue.push_back(aMessage);
//     Serial << "[Link] [enqueue] end" << endl;    
//  }
// =====================================================================================================================
MqttLink::MqttLink(/* args */){
    // WiFiClient espClient;
    this->espClient = new WiFiClient();
    WiFi.mode(WIFI_STA);
    WiFi.begin("CPX", "suavesuavecito");

    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial << "." ;
    }

    Serial << "[MqttLink] [MqttLink] WiFi connected" << " IP address: " << WiFi.localIP() << endl;

    this->pubsub = new  PubSubClient(*(this->espClient));
    this->pubsub->setServer("192.168.100.16", 1883);  
    this->pubsub->setBufferSize(2500);
    if (this->pubsub->connect("espnode-" )) {
        Serial << "connected rc=" << pubsub->state() << endl;
    }
    this->pubsub->setCallback(mqttCallback);

    int rc = this->pubsub->publish("outTopic2", "msg");
    Serial << "[MqttLink] @@@@@@@@ [MqttLink] published: "  << rc << endl;    
}
// ---------------------------------------------------------------------------------------------------------------------
void MqttLink::subscribe_topics(){
    Serial << "[MqttLink] [subscribe_topics] "  << endl;
    Serial << "[MqttLink] [subscribe_topics] command"  <<  this->topics->command << endl;
    this->pubsub->subscribe(this->topics->command);
}
// ---------------------------------------------------------------------------------------------------------------------
void MqttLink::mqttCallback(char* topic, byte* payload, unsigned int length) {
    /**************\
     *   STATIC  * |
    \**************/ 
    Serial << "[MqttLink] [mqttCallback] 1.0 MQTT Message arrived " <<endl;
    Serial << "[MqttLink] [mqttCallback] 1.0 MQTT Message arrived topic       = " << topic << endl;
    String stringTopic = String(topic); //  bug ici
    Serial << "[MqttLink] [mqttCallback] 1.1 MQTT Message arrived stringTopic = " << topic << endl;
    char* cPayload = new char[100];
    cPayload = (char*)payload;
    cPayload[length]=0;

    Link::device->incomingMessage(topic, cPayload);
}
// ---------------------------------------------------------------------------------------------------------------------
void MqttLink::enqueue(Message aMessage){
    Serial << "[MqttLink] [enqueue] " << aMessage.channel << " - " << aMessage.payload << endl;    
    Serial << "[MqttLink] [enqueue] " << endl;    
    this->message_queue.push_back(aMessage);
}
// ---------------------------------------------------------------------------------------------------------------------
void MqttLink::publish(char * aChannel, char * aPayload){
    device->led_notification(HIGH);
    Serial << "[MqttLink] [publish] Channel: " << aChannel << " message=" << aPayload << endl;
    if (this->pubsub == NULL){
        Serial << "[MqttLink] [publish] pubsub NULL " << endl;  
    }
    int rc = this->pubsub->publish(aChannel, aPayload);
    Serial << "[MqttLink] [publish] @@@@@@@@  published: "  << rc << endl; 
    device->led_notification(LOW);
}
// ---------------------------------------------------------------------------------------------------------------------
void MqttLink::display(){
    Serial << "[MqttLink] [display] MqttLink" << endl;
}
// ---------------------------------------------------------------------------------------------------------------------
void MqttLink::setTopics(Topics * aTopics){
    this->topics = aTopics;
    this->subscribe_topics();
    Serial << "[MqttLink] [setTopics] topics.hello : " << this->topics->hello << endl;
    Serial << "[MqttLink] [setTopics] topics.command : " << this->topics->command << endl;
    Serial << "[MqttLink] [setTopics] topics.feedback : " << this->topics->feedback << endl;
    Serial << "[MqttLink] [setTopics] topics.response : " << this->topics->response << endl;
    Serial << "[MqttLink] [setTopics] topics.telemetry : " << this->topics->telemetry << endl;
    Serial << "[MqttLink] [setTopics] topics.debug : " << this->topics->debug << endl;
    Serial << "[MqttLink] [setTopics] topics.error : " << this->topics->error << endl;
}
// ---------------------------------------------------------------------------------------------------------------------
void MqttLink::loop(){
    yield();
    while (this->message_queue.size() > 0){
        Message m = message_queue[0];
        Serial << "[MqttLink] [loop] unqueue : " << m.channel << " - " << m.payload << endl;
        char * channel = (char *)malloc(m.channel.length());
        char * payload = (char *)malloc(m.payload.length());
        m.channel.toCharArray(channel, m.channel.length()+1);
        m.payload.toCharArray(payload, m.payload.length()+1);
        Serial << "[MqttLink] [loop] **** unqueue char* : " << channel << " - " << payload << endl;
        message_queue.pop_back();

        this->publish(channel, payload);
    }
    this->pubsub->loop();
    yield();
}
// =====================================================================================================================
LoraLink::LoraLink(/* args */){
}
// ---------------------------------------------------------------------------------------------------------------------
void LoraLink::publish(char * aChannel, char * aMessage){
}
// ---------------------------------------------------------------------------------------------------------------------
void LoraLink::display(){
    Serial << "[LoraLink] [display] LoraLink" << endl;
}
// =====================================================================================================================
BluetoothLink::BluetoothLink(/* args */){
    this->SerialBT = new BluetoothSerial();
    this->SerialBT->begin("Esp Node"); //Bluetooth device name
}
// ---------------------------------------------------------------------------------------------------------------------
void BluetoothLink::publish(char * aChannel, char * aPayload){
    Serial << "[BluetoothLink] [publish]  " << aChannel << " - " << aPayload << endl; 
    size_t fullSize= strlen(aChannel) + 1 + strlen(aPayload) + 1;
    char * fullMessage;
    fullMessage = (char *) malloc( fullSize );
    strcpy(fullMessage, aChannel );
    strcat(fullMessage, "~" );
    strcat(fullMessage, aPayload );
    Serial << "[BluetoothLink] [publish] fullMessage " << fullMessage << endl; 
    this->SerialBT->println(fullMessage);
    //free(fullMessage);
}
// ---------------------------------------------------------------------------------------------------------------------
void BluetoothLink::display(){
    Serial << "[BluetoothLink] [display] BluetoothLink" << endl;
}
// ---------------------------------------------------------------------------------------------------------------------
void BluetoothLink::enqueue(Message aMessage){
    Serial << "[BluetoothLink] [enqueue] **********************" << endl;    
    Serial << "[BluetoothLink] [enqueue] " << aMessage.channel << " - " << aMessage.payload << endl;    
    // // this->message_queue.push_back(aMessage);
    char * chChannel = (char*)malloc(aMessage.channel.length()+1);
    strcpy(chChannel, aMessage.channel.c_str());
    char * chPayload = (char*)malloc(aMessage.payload.length()+1);
    strcpy(chPayload, aMessage.payload.c_str());
    this->publish(chChannel, chPayload);
    free(chChannel);
    free(chPayload);
}
// ---------------------------------------------------------------------------------------------------------------------
void BluetoothLink::setTopics(Topics * aTopics){

}
// ---------------------------------------------------------------------------------------------------------------------
void BluetoothLink::loop(){
    yield(); 
    String message = "";
    String topic = "";
    String payload = "";
    bool mesg_received = false;
    bool separator_found = false;
    while (SerialBT->available()) {
        char a = SerialBT->read();
        message = message + a;
        if (a=='~'){
            Serial << "[BluetoothLink] [loop] separator found" << endl;
            separator_found = true;
            a = SerialBT->read();
        }
        if (not separator_found){
            topic = topic + a;
        }else{
            payload = payload + a;
        }
        mesg_received = true;
    }
    if (mesg_received){
        Serial << "[BluetoothLink] [loop] message received - topic: " << topic << " - payload: " << payload << endl;
        char * chChannel = (char*)malloc(topic.length()+1);
        strcpy(chChannel, topic.c_str());
        char * chPayload = (char*)malloc(payload.length()+1);
        strcpy(chPayload, payload.c_str());
        Link::device->incomingMessage(chChannel, chPayload);
        free(chChannel);
        free(chPayload);
    }
}
// =====================================================================================================================

#include <Ticker.h>
#include <Streaming.h>
#include <ArduinoJson.h>          //https://github.com/bblanchon/ArduinoJson

#include "telemetries.h"

class Measure;
// =====================================================================================================================
Telemetry::Telemetry(Module * aModule, String aTelemetryKey, String aTelemetry_desc){
    this->key = aTelemetryKey;
    this->desc = aTelemetry_desc;
    this->owner = aModule;
    this->measures.setStorage(this->measures_storage_array);
    this->ticker = new Ticker();
}
// ---------------------------------------------------------------------------------------------------------------------
void Telemetry::add_measure(Measure *aMeasure){
    Serial << "++++++++++++++++  " << aMeasure->getKey() << endl;
    this->measures.push_back(aMeasure);
}
// ---------------------------------------------------------------------------------------------------------------------
void Telemetry::print(){
}
// ---------------------------------------------------------------------------------------------------------------------
String Telemetry::toJson(){
    Serial << "---"<< endl;
    Serial << this->key  << " " << this->desc << endl;
    String telemetryDump = "{ \"key\":\"" +  this->key + "\",  \"desc\":\"" +  this->desc + "\", \"measures\": [";
    bool add_comma = false;
    for (int i=0; i<this->measures.size(); i++){
        Serial << "       Clé:   " << this->measures[i]->getKey() << "   ( " << this->measures[i]->getDesc() << ")" << endl;
        if (add_comma){ telemetryDump = telemetryDump + ","; }else{ add_comma = true; }
        telemetryDump = telemetryDump + "{\"key\": \"" +  this->measures[i]->getKey() + "\", \"desc\": \"" + this->measures[i]->getDesc() + "\"}";
    }
    telemetryDump = telemetryDump + "]}";
    return telemetryDump;
}
// ---------------------------------------------------------------------------------------------------------------------
String Telemetry::getKey(){
    return this->key;
}
// ---------------------------------------------------------------------------------------------------------------------
String Telemetry::getDesc(){
    return this->desc;
}
// ---------------------------------------------------------------------------------------------------------------------
Ticker * Telemetry::getTicker(){
    return this->ticker;
}
// ---------------------------------------------------------------------------------------------------------------------
void Telemetry::register_measure(String aKey, String aDesc, String aUnit){
    this->add_measure(new Measure(aKey, aDesc, aUnit));
}
// ---------------------------------------------------------------------------------------------------------------------
Measure * Telemetry::getMeasure(String aMeasureKey){
     for(int i=0; i<this->measures.size(); i++){
        if (aMeasureKey == this->measures[i]->getKey()){
            Serial << "[Telemetry] [getMeasure] - Measure: " << aMeasureKey << " found" << endl;
            return this->measures[i];
        }
    }
    Serial << "[Telemetry] [getMeasure] - Measure: " << aMeasureKey << " not found" << endl;
    return NULL;
}
// ---------------------------------------------------------------------------------------------------------------------
String Telemetry::formatJsonFrame(){

    char jsonData[JSON_MSG_SZ];
    StaticJsonDocument<400> doc;
    JsonObject root = doc.to<JsonObject>();
     for(int i=0; i<this->measures.size(); i++){
        String measure_key = this->measures[i]->getKey();
        float measure_value = this->measures[i]->getValue();
        Serial << "[Telemetry] [formatJsonFrame] - measure_key: " << measure_key << " = " << measure_value << endl;
        root[measure_key] = measure_value;
    }
    serializeJson(doc, jsonData);
    String jsonDataStr = String(jsonData);
    Serial << "[Telemetry] [formatJsonFrame] - jsonDataStr: " << jsonDataStr << endl;
    return jsonDataStr;
}
// =====================================================================================================================
Measure::Measure(String aKey, String aDesc, String aUnit){
    this->key = aKey;
    this->desc = aDesc;
    this->unit = aUnit;
}
// ---------------------------------------------------------------------------------------------------------------------
Measure::Measure(){
}
// ---------------------------------------------------------------------------------------------------------------------
void Measure::setValue(float aValue){
    this->value = aValue;
}
// ---------------------------------------------------------------------------------------------------------------------
float Measure::getValue(){
    return this->value;
}
// ---------------------------------------------------------------------------------------------------------------------
String Measure::getKey(){
    return this->key;
}
// ---------------------------------------------------------------------------------------------------------------------
String Measure::getDesc(){
    return this->desc;
}

// =====================================================================================================================

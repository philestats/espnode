#ifndef COMMANDS_H
#define COMMANDS_H

#include <Arduino.h>
#include <Vector.h>
#include <ArduinoJson.h>          //https://github.com/bblanchon/ArduinoJson
// max nb of arguments in a command
#define ARGUMENTS_MAX   5

class Module;
// =====================================================================================================================
struct Command_arg{
private:
public:
    // an argument is identified by a pair key+value
    // order:OPEN is an argument
    // order:CLOSE is another argument
    // the key of the argument. ie: "order"
    String key;
    String desc;
    String value;
    // order:OPEN
    Command_arg& setKey(String aKey) { key = aKey; return *this;}
    Command_arg& setDesc(String aDesc) { desc = aDesc; return *this;}
    Command_arg& setValue(String aValue) { value = aValue; return *this;}
    // utiliser une value = $INT$ pour passer une value variable entier / $FLOAT$ / $STRING$
};
// =====================================================================================================================
//typedef  *DynamicJsonDocument pDynamicJsonDocument;
//typedef String (*command_callback)(String);
typedef String (*command_callback)(DynamicJsonDocument * jsonArguments);
// =====================================================================================================================
class Command{
/* A command has several arguments
Example: command-name":"cumulus","command-desc":"Controle le chauffe eau","command-args":"{\"status\":\"statut\",\"on\":\"allumer\",\"off\":\"eteindre\"}"}
*/
    private:
        int nb_args;
        String key;
        String desc;
        //Command_arg args[1];
        Command_arg args_storage_array[ARGUMENTS_MAX];
        // the owner is the module
        Module * owner;
    public:
        Command(Module * aModule, String aCommand_name, String aCommand_desc);
        Command * add_argument(Command_arg arg);
        void print();
        String toJson();
        String getKey();
        String getDesc();
        String executeCmd(DynamicJsonDocument * jsonDocArgs);
        String checkArguments(String arguments, DynamicJsonDocument * doc);
        command_callback execute;
        Vector<Command_arg> arguments;
};
// =====================================================================================================================
class Commands_list{
    private:
        int nb_cmds;
    public:
        Commands_list();
        Commands_list add_command(Command cmd);
        int a;
        Command commands[];
        void print();
};
// =====================================================================================================================
#endif
#include <Streaming.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>          //https://github.com/bblanchon/ArduinoJson

#include "module.h"

#include "hermes_protocol.h"

// ---------------------------------------------------------------------------------------------------------------------
// Members declared static in the header file are recalled here
Module * Module::instance;
String Module::current_command_uid;

// ---------------------------------------------------------------------------------------------------------------------
void Module::init(){
    Serial << "[Module] init "  << endl;
    instance = this;
    this->commands.setStorage(this->commands_storage_array);
    this->telemetries.setStorage(this->telemetries_storage_array);
    this->states.setStorage(this->states_storage_array);
    this->setup_hardware();
    this->setup_commands();
    this->setup_telemetry();
    this->setup_states();
}
// ---------------------------------------------------------------------------------------------------------------------
Module::Module(){
    Serial << "[Module] [Module] " << endl;
}
// ---------------------------------------------------------------------------------------------------------------------
Module * Module::getInstance(){
    return Module::instance;
}
// ---------------------------------------------------------------------------------------------------------------------
void Module::setName(String aName){
    this->name = aName;
}
// ---------------------------------------------------------------------------------------------------------------------
String Module::getName(){
    return this->name;
}
// ---------------------------------------------------------------------------------------------------------------------
void Module::setDescr(String aDesc){
    this->descr = aDesc;
}
// ---------------------------------------------------------------------------------------------------------------------
String Module::getDescr(){
    return this->descr;
}
// ---------------------------------------------------------------------------------------------------------------------
Command * Module::findCommand(String command_key){
    for(int i=0; i<this->commands.size(); i++){
        if (command_key == this->commands[i]->getKey()){
            Serial << "[Module] [findCommand] - Command: " << command_key << " found" << endl;
            return this->commands[i];
        }
    }
    Serial << "[Module] [findCommand] - Command: " << command_key << " not found" << endl;
    return NULL;
}
// ---------------------------------------------------------------------------------------------------------------------
void Module::print(){
    Serial << "[Module] [print] - " << this->name << endl;
    for (int i=0; i<this->commands.size(); i++){
        this->commands[i]->print();
    }
}
// ---------------------------------------------------------------------------------------------------------------------
String Module::processCommand(Command * aCommand, DynamicJsonDocument * jsonDocArgs){
    // This method is the entypoint of the processing of the commands
    // TODO envoyer une notif: command pending
    Serial << "[Module] [processCommand] - topicFeedback = " << this->topics.feedback << endl;

    String command = aCommand->getKey();
    return aCommand->execute(jsonDocArgs);
}
// ---------------------------------------------------------------------------------------------------------------------
void Module::register_command_uid(String uid){
    // record a command uid as the current command handled by the module
    Module::current_command_uid = uid;
    Serial << "[Module] [register_command_uid] current_command_uid = " << Module::current_command_uid << endl;
}
// ---------------------------------------------------------------------------------------------------------------------
void Module::un_register_command_uid(const char* uid){
    Module::current_command_uid = "";
    Serial << "[Module] [un_register_command_uid] current_command_uid = " << uid << endl;
}
// ---------------------------------------------------------------------------------------------------------------------
String Module::get_command_uid(){
    return Module::current_command_uid;
}
// ---------------------------------------------------------------------------------------------------------------------
void Module::send_pulse(int pin, int direction, int duration){
    // put the pin at state direction (HIGH or LOW) during duration milliseconds, then put the pin at the opposite  direction
    Serial << "[Module] [send_pulse] - pin: " << pin << " direction: " << direction << " duration: " << duration << endl;
    digitalWrite(pin, direction); 
    delay(duration);
    digitalWrite(pin, !direction);
}
// ---------------------------------------------------------------------------------------------------------------------
void Module::setModuleList(ModuleList * aModuleList){
    this->moduleList = aModuleList;
}
// ---------------------------------------------------------------------------------------------------------------------
void Module::publish(Message aMessage){
    // this->moduleList->generic_publish(aChannel, aMessage);
    Serial << "[Module] [publish] " << aMessage.channel << " - " << aMessage.payload << endl;
    this->moduleList->device->enqueue(aMessage);
}
// ---------------------------------------------------------------------------------------------------------------------
void Module::enqueue(Message aMessage){
    Serial << "[Module] [enqueue] " << aMessage.channel << " - " << aMessage.payload << endl;
    this->moduleList->device->enqueue(aMessage);
}
// =====================================================================================================================
void Module::enqueue_feedback(String command_uid, const char* command_result){
    StaticJsonDocument<400> doc;
    JsonObject root = doc.to<JsonObject>();
    root["uid"] = command_uid;
    root["feedback"] = command_result;
    char jsonFeedback[JSON_MSG_SZ];
    serializeJson(doc, jsonFeedback);

    Message m;
    m.channel = this->moduleList->device->topics.feedback;
    m.payload =  String(jsonFeedback);

    Serial << "[Module] [enqueue_feedback] ***************************" << endl;
    Serial << "[Module] [enqueue_feedback] " << m.channel << " - " << m.payload << endl;
    this->moduleList->device->enqueue(m);
}
// ---------------------------------------------------------------------------------------------------------------------
Wiring Module::getWiringForPinId(int wiringId){
   for (int i=0; i<sizeof(this->wiring); i++){
       if (this->wiring[i].id == wiringId){
           return this->wiring[i];
       }
   }
}
// =====================================================================================================================
void Module::setTopics(Topics * aTopics){
    this->topics = *aTopics;
}
// ---------------------------------------------------------------------------------------------------------------------
ModuleList::ModuleList(){
    this->modules.setStorage(this->module_storage_array);
}
// ---------------------------------------------------------------------------------------------------------------------
void ModuleList::add(Module *aModule){
    Serial << "[ModuleList] [add_Module] " << aModule->getName() << endl;
    this->modules.push_back(aModule);
    aModule->setTopics(this->topics);
    aModule->setModuleList(this);
}
// ---------------------------------------------------------------------------------------------------------------------
void ModuleList::print(){
    Serial << "[ModuleList] [print] " << endl;
    for(int i=0; i<this->modules.size(); i++){
        Serial << "Module #" << i << endl;
        this->modules[i]->print();
    }
}
// ---------------------------------------------------------------------------------------------------------------------
void ModuleList::setTopics(Topics * aTopics){
    this->topics = aTopics;
}
// ---------------------------------------------------------------------------------------------------------------------
String ModuleList::getSpecifications(String uid){
    Serial << "[ModuleList]  [getSpecifications] " << endl;

    //int maindoc_capacity=JSON_ARRAY_SIZE(100)+2*JSON_OBJECT_SIZE(100);
    const int maindoc_capacity = 4000;
    // Serial << "taille " << maindoc_capacity << endl;

    StaticJsonDocument<maindoc_capacity>maindoc;
    maindoc["uid"] = uid;
    maindoc["type"] = "specifications";
    JsonArray modules = maindoc.createNestedArray("modules");
    for(int modulecounter=0; modulecounter<this->modules.size(); modulecounter++){
        Module * current_module = this->modules[modulecounter];

        JsonObject moduleJsonObject = modules.createNestedObject();
        moduleJsonObject["modulename"] = current_module->getName();
        moduleJsonObject["moduledescr"] = current_module->getDescr();
        JsonArray commandsJsonArray = moduleJsonObject.createNestedArray("commands");
        for (int commandcounter=0; commandcounter<current_module->commands.size(); commandcounter++){
            commandsJsonArray[commandcounter]["name"] = current_module->commands[commandcounter]->getKey();
            commandsJsonArray[commandcounter]["desc"] = current_module->commands[commandcounter]->getDesc();
            for (int argumentcounter=0; argumentcounter<current_module->commands[commandcounter]->arguments.size(); argumentcounter++){
                commandsJsonArray[commandcounter]["arguments"][argumentcounter]["key"] = current_module->commands[commandcounter]->arguments[argumentcounter].key;
                commandsJsonArray[commandcounter]["arguments"][argumentcounter]["value"] = current_module->commands[commandcounter]->arguments[argumentcounter].value;
                commandsJsonArray[commandcounter]["arguments"][argumentcounter]["desc"] = current_module->commands[commandcounter]->arguments[argumentcounter].desc;
            }
        }

        JsonArray telemetriesJsonArray = moduleJsonObject.createNestedArray("telemetries");
        for (int telemetrycounter=0; telemetrycounter<current_module->telemetries.size(); telemetrycounter++){
            telemetriesJsonArray[telemetrycounter]["name"] = current_module->telemetries[telemetrycounter]->getKey();
            telemetriesJsonArray[telemetrycounter]["desc"] = current_module->telemetries[telemetrycounter]->getDesc();
            for (int measurecounter=0; measurecounter<current_module->telemetries[telemetrycounter]->measures.size(); measurecounter++){
                telemetriesJsonArray[telemetrycounter]["measures"][measurecounter]["key"] = current_module->telemetries[telemetrycounter]->measures[measurecounter]->getKey();
                telemetriesJsonArray[telemetrycounter]["measures"][measurecounter]["desc"] = current_module->telemetries[telemetrycounter]->measures[measurecounter]->getDesc();
            }
        }

        JsonArray statesJsonArray = moduleJsonObject.createNestedArray("states");
        for (int statecounter=0; statecounter<this->modules[modulecounter]->states.size(); statecounter++){
            statesJsonArray[statecounter]["name"] = current_module->states[statecounter]->getKey();
            statesJsonArray[statecounter]["desc"] = current_module->states[statecounter]->getDesc();
        }

    }

    char jsonSpecs[1500];
    size_t sz = 1500;
    serializeJson(maindoc, jsonSpecs, sz);
    String stringJsonSpecs(jsonSpecs);
    // missing } at the end of the serialized json: add it
    stringJsonSpecs = stringJsonSpecs + "}";
    Serial << "[ModuleList] [getSpecifications] stringJsonSpecs = " << stringJsonSpecs << endl;
    return stringJsonSpecs;
}
// ---------------------------------------------------------------------------------------------------------------------
Module * ModuleList::findModule(String module_name){
    // this method scans the module list to get the module with the name passed as parameter
    // if  a module is found, returns a pointer too this module
    // if no module is found, returns NULL
    for(int i=0; i<this->modules.size(); i++){
        if (module_name == this->modules[i]->getName()){
            Serial << "[ModuleList] [findModule] - Module: " << module_name << " found" << endl;
            return this->modules[i];
        }
    }
    Serial << "[ModuleList] [findModule] - Module: " << module_name << " not found" << endl;
    return NULL;
}
// ---------------------------------------------------------------------------------------------------------------------
String ModuleList::forwardCommand(String module_name, String command_name, String arguments){
    // this method searches a module named as parameter module_name
    // if the module is found, it searches for a command command_name in the module
    // then the command is run
    Serial << "===============================================================" << endl;
    Serial << "[ModuleList] [forwardCommand] - forward command - module: " << module_name <<  " command_name: " << command_name << " arguments: " << arguments << endl;
    Module * moduleDest = this->findModule(module_name);
    if (moduleDest == NULL){
        return COMMAND_MODULE_NOT_FOUND;
    }else{
        Command * commandDest = moduleDest->findCommand(command_name);
        if (commandDest == NULL){
            return COMMAND_NOT_FOUND;
        }else{
            // Module found, command found
            DynamicJsonDocument jsonArguments(1024);
            // check the arguments: the jsonArguments arduinoJson is filed by the deserialized arguments if the returned value is COMMAND_ARGS_OK
            String res = commandDest->checkArguments(arguments, &jsonArguments);
            if (res == COMMAND_ARGS_OK){
                res = commandDest->executeCmd(&jsonArguments);
                Serial << "[ModuleList] [forwardCommand] - Command result: " << res << endl;
                return res;
            }else{
                return res;
            }
            jsonArguments.clear();
        }
    }
}
// ---------------------------------------------------------------------------------------------------------------------
void ModuleList::generic_publish(char * aChannel, char * aMessage){
    Serial << "[ModuleList] [generic_publish] - aChannel: " << aChannel << " aMessage: " << aMessage << endl;
    this->device->publish(aChannel, aMessage);
}
// ---------------------------------------------------------------------------------------------------------------------
void ModuleList::setDevice(Device * aDevice){
    this->device = aDevice;
}
// ---------------------------------------------------------------------------------------------------------------------
void ModuleList::publish_telemetry(Telemetry * aTelemetry, String aMessage){
    Serial << "[ModuleList] [publish_telemetry] " << endl;
    char topic[64];

    sprintf(topic, "%s/%s/%s", this->device->topics.telemetry, aTelemetry->owner->getName(), aTelemetry->getKey());
    Serial << "[ModuleList] [publish_telemetry] topic = " << topic << endl;
    char * mesg = (char *)aMessage.c_str();
    Serial << "[ModuleList] [publish_telemetry] mesg  = " << mesg << endl;
    this->device->publish(topic, mesg);
}
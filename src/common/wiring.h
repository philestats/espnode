#ifndef WIRING_H
#define WIRING_H

struct Wiring{
private:
public:
    String desc;
    int pin;
    int id;
    Wiring& setPin(int aPin) { pin = aPin; return *this;}
    Wiring& setDesc(String aDesc) { desc = aDesc; return *this;}
    Wiring& setId(int aId) { id = aId; return *this;}
};

#endif
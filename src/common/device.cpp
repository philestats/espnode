#include <Streaming.h>
#include <ArduinoJson.h>          //https://github.com/bblanchon/ArduinoJson

#include <SPI.h>
#include <LoRa.h>

#include "device.h"
#include "commands.h"
#include "hermes_protocol.h"

#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>

#ifdef HAS_LORA
//define the pins used by the LoRa transceiver module
#define SCK 5
#define MISO 19
#define MOSI 27
#define SS 18
#define RST 14
#define DIO0 26
#define BAND 866E6
#endif

#ifdef HAS_OLED_DISPLAY
//OLED pins
#define OLED_SDA 4
#define OLED_SCL 15 
#define OLED_RST 16
#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 32 // OLED display height, in pixels
#endif
 



Device * Device::instance; // here the static member is redeclared 
int Device::debug_nb;
// ---------------------------------------------------------------------------------------------------------------------
void Device::display_message(char * aMessage){
    Serial << aMessage << endl;
    #ifdef HAS_OLED_DISPLAY
    // this->display.clearDisplay();
    // this->display.setTextColor(WHITE);
    // this->display.setTextSize(1);
    // this->display.setCursor(0,0);
    // this->display.print(aMessage);
    // this->display.display();
    // delay(1);



      // init done
  this->display.clearDisplay();
//   // text display tests
//   this->display.setTextSize(1);
//   this->display.setTextColor(WHITE);
//   this->display.setCursor(0,0);
//   this->display.println("Hello");
  this->display.println("Hello World");
//   this->display.setTextColor(BLACK, WHITE); // 'inverted' text
//   this->display.println(3.141592);
//   this->display.setTextSize(2);
//   this->display.setTextColor(WHITE);
//   this->display.print("0x"); 
//   this->display.println(0xDEADBEEF, HEX);
  this->display.display();


    #endif
}
// ---------------------------------------------------------------------------------------------------------------------
void Device::setup_lora(){
    #ifdef HAS_LORA
    //SPI LoRa pins
    SPI.begin(SCK, MISO, MOSI, SS);
    //setup LoRa transceiver module
    LoRa.setPins(SS, RST, DIO0);
    LoRa.setFrequency(868100000);
    LoRa.setSpreadingFactor(7);
    LoRa.setSignalBandwidth(250E3);
    LoRa.setCodingRate4(8);
    if (!LoRa.begin(BAND)) {
        Serial << "Starting LoRa failed!" << endl;
        while (1);
    }
    Serial << "LoRa Initializing OK!" << endl;
    display.setCursor(0,10);
    display.print("LoRa Initializing OK!");
    #endif
}
// ---------------------------------------------------------------------------------------------------------------------
void Device::setup_display_SSD1306(){
    #ifdef HAS_OLED_DISPLAY
    this->display = Adafruit_SSD1306(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RST);

    //reset OLED display via software
    pinMode(OLED_RST, OUTPUT);
    digitalWrite(OLED_RST, LOW);
    delay(20);
    digitalWrite(OLED_RST, HIGH);

    //initialize OLED
    Wire.begin(OLED_SDA, OLED_SCL);
    if(!this->display.begin(SSD1306_SWITCHCAPVCC, 0x3c, false, false)) { // Address 0x3C for 128x32
        Serial.println(F("SSD1306 allocation failed"));
        for(;;); // Don't proceed, loop forever
    }
    this->display_message("Salut ! :-)");
    #endif
}
// ---------------------------------------------------------------------------------------------------------------------
Device::Device(String aName, String aMacAddress, Wiring * wiring){
    Serial << "[Device] [Device] "  << endl;
    Device::debug_nb = 1;
    instance = this;
    this->name = aName;
    this->macaddr = aMacAddress;
    this->setup_topics();
    this->moduleList = new ModuleList();
    this->moduleList->setDevice(this);
    this->moduleList->setTopics(&(this->topics));
    // quand cette ligne est active, le bme280 n'est pas trouvé
    //this->setup_display_SSD1306();
    this->wiring = wiring;
    led_notification_pin = wiring[0].pin;
    pinMode(led_notification_pin, OUTPUT);
}
// ---------------------------------------------------------------------------------------------------------------------
void Device::loop(){
    /*
     Device main loop
     */
    while (true){
        // process the link loop
        this->link->loop();
        yield();

    }       
}
// ---------------------------------------------------------------------------------------------------------------------
void Device::setCommunicationLink(Link * aLink){
    this->link = aLink;
    this->link->setDevice(this);
    this->link->setTopics(&(this->topics));
    this->sendHello();
}
// ---------------------------------------------------------------------------------------------------------------------
void Device::registerModule(Module * aModule){
    Serial << "[Device] [registerModule] " << aModule->getName() << endl;
    this->moduleList->add(aModule);
    aModule->init();
}
// ---------------------------------------------------------------------------------------------------------------------
void Device::enqueue(Message aMessage){
    Serial << "[Device] [enqueue] " << aMessage.channel << " - " << aMessage.payload << endl;
    this->link->enqueue(aMessage);
}
// ---------------------------------------------------------------------------------------------------------------------
void Device::publish(char * aChannel, char * aMessage){
    Serial << "[Device] [publish] Channel: " << aChannel << " message=" << aMessage << endl;
    yield(); 
    this->link->publish(aChannel, aMessage);   
}
// ---------------------------------------------------------------------------------------------------------------------
void Device::sendDebug(char* aMesg){
    // Serial << "[Device] [sendDebug] message=" << aMesg << endl;
    // char debugMsg[256];
    // sprintf(debugMsg, "[%s] %s", Device::debug_nb, aMesg);

    // instance->publish(instance->topics.debug, debugMsg);
    // Device::debug_nb ++;
}
// ---------------------------------------------------------------------------------------------------------------------
void Device::sendHello(){
    /*
     * the device sends an announcement with its basic description
     */
    Serial << "[device] [sendHello]" << endl;
    StaticJsonDocument<400> doc;
    JsonObject root = doc.to<JsonObject>();

    root["id"] = this->macaddr;
    root["name"] = this->name;
    root["sw"] = SOFTWARE_NAME;
    root["ver"] = SOFTWARE_VERSION;
    //root["loopmode"] = loop_mode;
    root["hw"] = HW_TYPE;
    root["communication"] = this->communication_medium;
    root["specs"] = "1";
    char jsonHello[JSON_MSG_SZ];
    // TODO ajouter un hash des spécifications
    serializeJson(doc, jsonHello);
    Serial << jsonHello << endl;

    this->publish(this->topics.hello, jsonHello);
}
// ---------------------------------------------------------------------------------------------------------------------
void Device::incomingMessage(char* topic, char* payload){
    // traitement des messages MQTT
    Serial << "[device] [incomingMessage] 1.1 MQTT Message arrived stringTopic = " << topic << endl;   
    Serial << "[device] [incomingMessage] 1.22 payload      =" << payload <<endl;

    // extraire le destmodule du topic
    // TOPIC: devices/command/<DEVICE>/<MODULE>/<COMMAND>
    // arguments et eventuelle value dans la paylod
    // Message: "{\"uid\":\"9487729\", \"arg\": \"status\"}"
    String stringTopic = String(topic); 
    Serial << "[device] [mqttCallback] 1.10 stringTopic  =" << stringTopic << endl;
    int slashIndex = stringTopic.indexOf('/');
    String str1 = stringTopic.substring(slashIndex+1);
    Serial << "[device] [mqttCallback] 1.11 str1         =" << str1 << endl;
   
    slashIndex = str1.indexOf('/');
    String str2 = str1.substring(slashIndex+1);

    slashIndex = str2.indexOf('/');
    String str3 = str2.substring(slashIndex+1);
    slashIndex = str3.indexOf('/');
    String destModule = str3.substring(0, slashIndex);
    Serial << "[device] [mqttCallback] 1.20 destModule   =" << destModule << endl;
    String command = str3.substring(slashIndex+1);
    Serial << "[device] [mqttCallback] 1.21 command      =" << command << endl;

    String stringPayload(payload);
    Serial << "[device] [mqttCallback] 1.22 payload      =" << stringPayload << "]" <<endl;

    StaticJsonDocument<JSON_MSG_SZ> doc;
    DeserializationError error = deserializeJson(doc, payload);

    if (error) {
        Serial << "[device] [mqttCallback] deserializeJson() failed: " << error.c_str() << endl;
        instance->publish(instance->topics.error, "dest: device");
    }
    const char* uid = doc["uid"];
    // dans l'appel de sendfeedback, publishtomqtt ecrase la valeur du char* uid. Je passe ici par un string qui n'est oas écrasé
    String stuid(uid);
    Serial << "[device] [mqttCallback] 1.23 uid         = " << stuid << endl;
    instance->sendFeedback(stuid, "CMD_RECEIVED");
    Serial << "[device] [mqttCallback] 3 apres feedback  -  uid :" << stuid << endl;

    if (destModule=="device"){
        // Technical messages to the device itself
        if (command=="specifications"){
            // request for device specifications
            instance->sendDebug("request specifications");
            //instance->publish(instance->topics.debug, "request specifications");
            instance->sendSpecifications(stuid);
        }else if (command=="reboot"){
            // request for rebooting the device
            Serial << "[device] [mqttCallback]  -  reboot" << endl;
            instance->sendFeedback(stuid, COMMAND_OK);
            ESP_reboot();
        }else if (command=="ota_upd"){
            // request for updating the device firmware
            Serial << "[device] [mqttCallback]  -  ota update" << endl;
            instance->sendFeedback(stuid, COMMAND_NYIMPLD);
            // not yet implemented
        }else{
            instance->sendFeedback(stuid, COMMAND_NOT_FOUND);            
        }
    }
    else{
        // the command destination is not the device itself
        // we forward it to the node list that will search for a node that is able to
        // process the command
        const char* arg = doc["arg"];
        Serial << "[mqttCallback]  -  arg :" << arg << endl;
        
        String commandStatus = instance->moduleList->forwardCommand(destModule, command, stringPayload);
        Serial << "[mqttCallback]  -  commandStatus:" << instance->topics.feedback << endl;
        instance->sendFeedback(stuid, commandStatus);
    }

}
// ---------------------------------------------------------------------------------------------------------------------
void Device::setup_topics(){
   
    strcpy(this->topics.hello, "devices/hello");
    sprintf(this->topics.command, "devices/command/%s/+/+", this->macaddr.c_str());
    sprintf(this->topics.feedback, "devices/feedback/%s", this->macaddr.c_str());
    sprintf(this->topics.response, "devices/response/%s", this->macaddr.c_str());
    sprintf(this->topics.telemetry, "devices/telemetry/%s", this->macaddr.c_str());
    sprintf(this->topics.debug, "devices/debug/%s", this->macaddr.c_str());
    sprintf(this->topics.error, "devices/error/%s", this->macaddr.c_str());

    Serial << "topics.hello : " << topics.hello << endl;
    Serial << "topics.command : " << topics.command << endl;
    Serial << "topics.feedback : " << topics.feedback << endl;
    Serial << "topics.response : " << topics.response << endl;
    Serial << "topics.telemetry : " << topics.telemetry << endl;
    Serial << "topics.debug : " << topics.debug << endl;
    Serial << "topics.error : " << topics.error << endl;

}
// ---------------------------------------------------------------------------------------------------------------------
void Device::sendSpecifications(String aCommandUid){
    Serial << "[device] [sendSpecifications] param aCommandUid = " << aCommandUid << endl;
    delay(1000);
    String stringJsonSpecs = this->moduleList->getSpecifications(aCommandUid);
    int n = stringJsonSpecs.length();
    // Serial << "[sendSpecifications] specs retour de modulelist->getSpecifications l= : " << n << " - " << stringJsonSpecs << endl;
   // this-sendDebug("specifications retrieved from moduleList");
    char atopic[100];
    sprintf(atopic, this->topics.response, "aCommandUid");
    Serial << "atopic " << atopic << endl;
    char * JsonSpecs = (char *)malloc(n);
    // je suis sur que le malloc marche :-)
    stringJsonSpecs.toCharArray(JsonSpecs, n);
    this->publish(this->topics.response, JsonSpecs);
    this->sendFeedback(aCommandUid, "CMD_EXECUTED");

    free(JsonSpecs);
}
// ---------------------------------------------------------------------------------------------------------------------
void Device::sendFeedback(String stuid,  String feedback){
    Serial << "[device] [sendFeedback] - [uid 1: " << stuid << "]"  << endl;
    StaticJsonDocument<400> doc;
    JsonObject root = doc.to<JsonObject>();
    root["uid"] = stuid;
    root["feedback"] = feedback;
    char jsonFeedback[JSON_MSG_SZ];
    serializeJson(doc, jsonFeedback);
    Serial << "[sendFeedback] - [uid 2: " << stuid << "]" << endl;
    // ICI le uid est écrasé par publishToMqtt, on ne sait pas pourquoi
    publish(this->topics.feedback, jsonFeedback);
    Serial << "[sendFeedback] - [uid 3: " << stuid << "]"  << endl;
}
// ---------------------------------------------------------------------------------------------------------------------
void Device::led_notification(int level){
    digitalWrite(led_notification_pin, level); 
}

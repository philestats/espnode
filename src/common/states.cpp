#include "states.h"
// https://github.com/janelia-arduino/Streaming
// https://github.com/janelia-arduino/Vector/
#include <Vector.h>
#include <Streaming.h>
#include "module.h"

// ---------------------------------------------------------------------------------------------------------------------
State::State(Module * aModule, String aStateKey, String aSateDesc){
    //Serial << "[Command] Constructor " << aCommand_name << endl;
    this->key = aStateKey;
    this->desc = aSateDesc;
    this->owner = aModule;
}
// ---------------------------------------------------------------------------------------------------------------------
String State::getKey(){
    return this->key;
}
// ---------------------------------------------------------------------------------------------------------------------
String State::getDesc(){
    return this->desc;
}
// ---------------------------------------------------------------------------------------------------------------------
void State::setValue(String aValue){
    this->value = aValue;
}
// ---------------------------------------------------------------------------------------------------------------------
String State::getValue(String aValue){
    return this->value;
}
// ---------------------------------------------------------------------------------------------------------------------
void State::print(){
    Serial << "---"<< endl;
    Serial << this->key  << " " << this->desc << endl;
}
// =====================================================================================================================

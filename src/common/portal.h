#ifndef PORTAL_H
#define PORTAL_H
#include <Ticker.h>
#include "hermes_protocol.h"

char configFileName[] = "/config.json";
// ---------------------------------------------------------------------------------------------------------------------
void saveConfigCallback(void);
bool loadSPIFFSConfigFile(void);
bool saveSPIFFSConfigFile(void);
// ---------------------------------------------------------------------------------------------------------------------
bool shouldSaveConfig = false;


//define your default values here, if there are different values in configFileName (config.json), they are overwritten.
//#define BLYNK_SERVER_LEN                64
//#define BLYNK_TOKEN_LEN                 32
//#define BLYNK_SERVER_PORT_LEN           6

#define MQTT_SERVER_MAX_LEN             40
#define MQTT_SERVER_PORT_LEN            6

// SSID and PW for your Router
String Router_SSID;
String Router_Pass;

// SSID and PW for Config Portal
String AP_SSID;
String AP_PASS;

//char blynk_server [BLYNK_SERVER_LEN]        = "account.duckdns.org";
//char blynk_port   [BLYNK_SERVER_PORT_LEN]   = "8080";
//char blynk_token  [BLYNK_TOKEN_LEN]         = "YOUR_BLYNK_TOKEN";

char mqtt_server  [MQTT_SERVER_MAX_LEN];
char mqtt_port    [MQTT_SERVER_PORT_LEN]    = "1883";

// 2 tickers: the 1st one switches on the led with a given period
Ticker tickerPortalAsym;
// the 2nd one switches it off
Ticker tickerPortalOff;
// ---------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------
void togglePortalLEDAsymOff(){
    digitalWrite(LED_STATUS, LED_OFF);
    tickerPortalOff.detach();
}
// ---------------------------------------------------------------------------------------------------------------------
void togglePortalLEDAsymON(){
    digitalWrite(LED_STATUS, LED_ON);
    tickerPortalOff.attach(0.05, togglePortalLEDAsymOff);
}
// ---------------------------------------------------------------------------------------------------------------------
void saveConfigCallback(void){
  //Serial.println("Should save config");
  shouldSaveConfig = true;
}
// ---------------------------------------------------------------------------------------------------------------------
//gets called when WiFiManager enters configuration mode
void configModeCallback (ESP_WiFiManager *myESP_WiFiManager)
{
    tickerPortalAsym.detach();
    Serial.println("Entered config mode with AP_SSID : " + myESP_WiFiManager->getConfigPortalSSID() + " and AP_PASS = " + myESP_WiFiManager->getConfigPortalPW());
    //entered config mode, make led toggle faster at 0.2s
    tickerPortalAsym.attach(0.1, togglePortalLEDAsymON);
}
// ---------------------------------------------------------------------------------------------------------------------
bool loadSPIFFSConfigFile(void){
  //clean FS, for testing
  //SPIFFS.format();

  //read configuration from FS json
  Serial.println("Mounting FS...");

  if (SPIFFS.begin())  {
    Serial.println("Mounted file system");

    if (SPIFFS.exists(configFileName))
    {
      //file exists, reading and loading
      Serial.println("Reading config file");
      File configFile = SPIFFS.open(configFileName, "r");

      if (configFile){
        Serial.print("Opened config file, size = ");
        size_t configFileSize = configFile.size();
        Serial.println(configFileSize);
        // Allocate a buffer to store contents of the file.
        std::unique_ptr<char[]> buf(new char[configFileSize + 1]);
        configFile.readBytes(buf.get(), configFileSize);
        Serial.print("\nJSON parseObject() result : ");

#if (ARDUINOJSON_VERSION_MAJOR >= 6)
        DynamicJsonDocument json(1024);
        auto deserializeError = deserializeJson(json, buf.get(), configFileSize);
        if ( deserializeError ){
          Serial.println("Deserialization of config file failed (1)");
          return false;
        }
        else{
          Serial.println("Deserialization of config file OK (1)");
//          if (json["blynk_server"])
//            strncpy(blynk_server, json["blynk_server"], sizeof(blynk_server));
//
//          if (json["blynk_port"])
//            strncpy(blynk_port, json["blynk_port"], sizeof(blynk_port));
//
//          if (json["blynk_token"])
//            strncpy(blynk_token,  json["blynk_token"], sizeof(blynk_token));

          if (json["mqtt_server"])
            strncpy(mqtt_server, json["mqtt_server"], sizeof(mqtt_server));

          if (json["mqtt_port"])
            strncpy(mqtt_port,   json["mqtt_port"], sizeof(mqtt_port));
        }

        //serializeJson(json, Serial);
        serializeJsonPretty(json, Serial);
#else
        DynamicJsonBuffer jsonBuffer;
        // Parse JSON string
        JsonObject& json = jsonBuffer.parseObject(buf.get());
        // Test if parsing succeeds.
        if (json.success()){
          Serial.println("Deserialization of config file OK (2)");

          if (json["blynk_server"])
            strncpy(blynk_server, json["blynk_server"], sizeof(blynk_server));

          if (json["blynk_port"])
            strncpy(blynk_port, json["blynk_port"], sizeof(blynk_port));

          if (json["blynk_token"])
            strncpy(blynk_token,  json["blynk_token"], sizeof(blynk_token));

          if (json["mqtt_server"])
            strncpy(mqtt_server, json["mqtt_server"], sizeof(mqtt_server));

          if (json["mqtt_port"])
            strncpy(mqtt_port,   json["mqtt_port"], sizeof(mqtt_port));
        }else{
            Serial.println("Deserialization of config file failed (2)");;
            return false;
        }
        //json.printTo(Serial);
        json.prettyPrintTo(Serial);
#endif

        configFile.close();
      }
    }
  }else{
    Serial.println("failed to mount FS");
    return false;
  }
  return true;
}
// ---------------------------------------------------------------------------------------------------------------------
bool saveSPIFFSConfigFile(void){
  Serial.println("Saving config");

#if (ARDUINOJSON_VERSION_MAJOR >= 6)
  DynamicJsonDocument json(1024);
#else
  DynamicJsonBuffer jsonBuffer;
  JsonObject& json = jsonBuffer.createObject();
#endif

//  json["blynk_server"] = blynk_server;
//  json["blynk_port"]   = blynk_port;
//  json["blynk_token"]  = blynk_token;

  json["mqtt_server"] = mqtt_server;
  json["mqtt_port"]   = mqtt_port;

  File configFile = SPIFFS.open(configFileName, "w");

  if (!configFile){
    Serial.println("Failed to open config file for writing");
  }

#if (ARDUINOJSON_VERSION_MAJOR >= 6)
  //serializeJson(json, Serial);
  serializeJsonPretty(json, Serial);
  // Write data to file and close it
  serializeJson(json, configFile);
#else
  //json.printTo(Serial);
  json.prettyPrintTo(Serial);
  // Write data to file and close it
  json.printTo(configFile);
#endif

  configFile.close();
  //end save
}
// ---------------------------------------------------------------------------------------------------------------------
boolean manageWifiConnection(bool do_start_portal){
    tickerPortalAsym.attach(0.4, togglePortalLEDAsymON);
    bool wifi_connected = false;
    // The extra parameters to be configured (can be either global or just in the setup)
    // After connecting, parameter.getValue() will get you the configured value
    // id/name placeholder/prompt default length
//    ESP_WMParameter custom_blynk_server("blynk_server", "blynk_server", blynk_server, BLYNK_SERVER_LEN + 1);
//    ESP_WMParameter custom_blynk_port  ("blynk_port",   "blynk_port",   blynk_port,   BLYNK_SERVER_PORT_LEN + 1);
//    ESP_WMParameter custom_blynk_token ("blynk_token",  "blynk_token",  blynk_token,  BLYNK_TOKEN_LEN + 1 );
    ESP_WMParameter custom_mqtt_server("mqtt_server", "mqtt_server", mqtt_server, MQTT_SERVER_MAX_LEN + 1);
    ESP_WMParameter custom_mqtt_port  ("mqtt_port",   "mqtt_port",   mqtt_port,   MQTT_SERVER_PORT_LEN + 1);

    // Use this to default DHCP hostname to ESP8266-XXXXXX or ESP32-XXXXXX
    //ESP_WiFiManager ESP_wifiManager;
    // Use this to personalize DHCP hostname (RFC952 conformed)
    ESP_WiFiManager ESP_wifiManager("AutoConnect-FSParams");

    //set config save notify callback
    ESP_wifiManager.setSaveConfigCallback(saveConfigCallback);
    ESP_wifiManager.setAPCallback(configModeCallback);
    //add all your parameters here
//    ESP_wifiManager.addParameter(&custom_blynk_server);
//    ESP_wifiManager.addParameter(&custom_blynk_port);
//    ESP_wifiManager.addParameter(&custom_blynk_token);

    ESP_wifiManager.addParameter(&custom_mqtt_server);
    ESP_wifiManager.addParameter(&custom_mqtt_port);

    //reset settings - for testing
    //ESP_wifiManager.resetSettings();

    ESP_wifiManager.setDebugOutput(true);

    //set minimu quality of signal so it ignores AP's under that quality
    //defaults to 8%
    //ESP_wifiManager.setMinimumSignalQuality();

    //set custom ip for portal
    ESP_wifiManager.setAPStaticIPConfig(IPAddress(192, 168, 100, 1), IPAddress(192, 168, 100, 1), IPAddress(255, 255, 255, 0));

    ESP_wifiManager.setMinimumSignalQuality(-1);
    // Set static IP, Gateway, Subnetmask, DNS1 and DNS2. New in v1.0.5+
    ESP_wifiManager.setSTAStaticIPConfig(IPAddress(192, 168, 2, 114), IPAddress(192, 168, 2, 1), IPAddress(255, 255, 255, 0),
                                       IPAddress(192, 168, 2, 1), IPAddress(8, 8, 8, 8));

    // We can't use WiFi.SSID() in ESP32 as it's only valid after connected.
    // SSID and Password stored in ESP32 wifi_ap_record_t and wifi_config_t are also cleared in reboot
    // Have to create a new function to store in EEPROM/SPIFFS for this purpose
    Router_SSID = ESP_wifiManager.WiFi_SSID();
    Router_Pass = ESP_wifiManager.WiFi_Pass();

    //Remove this line if you do not want to see WiFi password printed
    Serial.println("\nStored: SSID = " + Router_SSID + ", Pass = " + Router_Pass);
    bool no_routerSsid_stored_in_SPIFS = false;
    if (Router_SSID != "") {
        //ESP_wifiManager.setConfigPortalTimeout(120); //If no access point name has been previously entered disable timeout.
        Serial.println("Got stored Credentials");
    }else{
        Serial.println("No stored Credentials");
        no_routerSsid_stored_in_SPIFS = true;
    }

    if (do_start_portal || no_routerSsid_stored_in_SPIFS){
        tickerPortalAsym.detach();
        tickerPortalAsym.attach(0.2, togglePortalLEDAsymON);
        // Get Router SSID and PASS from EEPROM, then open Config portal AP named "ESP_XXXXXX_AutoConnectAP" and PW "MyESP_XXXXXX"
        // 1) If got stored Credentials, Config portal timeout is 60s
        // 2) If no stored Credentials, stay in Config portal until get WiFi Credentials
        String chipID = String(ESP_getChipId(), HEX);
        chipID.toUpperCase();
        Serial.println("2");
        // SSID and PW for Config Portal
        AP_SSID = "ESP_" + chipID + "_AutoConnectAP";
        //AP_PASS = "MyESP_" + chipID;
        if (!ESP_wifiManager.autoConnect(AP_SSID.c_str(), AP_PASS.c_str())) {
            Serial.println("Failed to setup an access point");
            //reset and try again
            ESP_reboot();
            delay(1000);
        }else{
            Serial.println("4");
            tickerPortalAsym.detach();
            return WiFi.status() == WL_CONNECTED;
        }
    }else{
        Serial.println("wifi connect");
        char router_SSID[32] = "";
        char router_Pass[32] = "";
        Router_SSID.toCharArray(router_SSID, 32);
        Router_Pass.toCharArray(router_Pass, 32);

        WiFi.begin(router_SSID, router_Pass);
        while (WiFi.status() != WL_CONNECTED) {
            delay(500);
            Serial.print(".");
        }
        tickerPortalAsym.detach();
        wifi_connected = true;
    }

    //read updated parameters
//    strncpy(blynk_server, custom_blynk_server.getValue(), sizeof(blynk_server));
//    strncpy(blynk_port,   custom_blynk_port.getValue(),   sizeof(blynk_port));
//    strncpy(blynk_token,  custom_blynk_token.getValue(),  sizeof(blynk_token));

    strncpy(mqtt_server, custom_mqtt_server.getValue(), sizeof(mqtt_server));
    strncpy(mqtt_port, custom_mqtt_port.getValue(),     sizeof(mqtt_port));

    //save the custom parameters to FS
    if (shouldSaveConfig){
        saveSPIFFSConfigFile();
    }
    tickerPortalAsym.detach();
    return wifi_connected;
}
// ---------------------------------------------------------------------------------------------------------------------
#endif
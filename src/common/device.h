#ifndef DEVICE_H
#define DEVICE_H

// TODO ces defines doive,t etre dans un fichier séparé
#define HAS_OLED_DISPLAY
#define HAS_LORA

#include <WiFiClient.h>
#include "Arduino.h"
#include <Streaming.h>
#include <PubSubClient.h>
#include <Ticker.h>
#include <PubSubClient.h>


#ifdef HAS_OLED_DISPLAY
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#endif


#ifdef HAS_LORA
#include <SPI.h>
#include <LoRa.h>
#endif

#include "hermes_protocol.h"
#include "module.h"
#include "link.h"
#include "message.h"
#include "wiring.h"

class ModuleList;
class Module;
class Link;
enum Communication_medium { WIFI_MQTT = 1, LORA = 2, BLUETOOTH = 3, BLE = 4 };
// =====================================================================================================================
// /!\ id of the wiring, not pin id 
#define DEVICE_WIRING_LED 0
// =====================================================================================================================
class Device {
private:
    Wiring * wiring;
    static int debug_nb; // this static member shall be redeclared in the C++ file  http://www.cs.technion.ac.il/users/yechiel/c++-faq/link-errs-static-data-mems.html
    static Device *instance; // this static member shall be redeclared in the C++ file  http://www.cs.technion.ac.il/users/yechiel/c++-faq/link-errs-static-data-mems.html
    //static PubSubClient * pubsub;
    static void mqttCallback(char* topic, byte* payload, unsigned int length);
    int led_notification_pin;
    String name;
    String macaddr;
    Communication_medium communication_medium;
    Link * link;
    String MqttClientId;
    ModuleList * moduleList;
    String get_printable_mac_address();
    #ifdef HAS_OLED_DISPLAY
    Adafruit_SSD1306 display = 0;//(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RST);
    #endif
    void setup_topics();
    //void setup_callback();
    void setup_display_SSD1306();
    void setup_lora();    
    //void subscribe_topics();
    void sendFeedback(String stuid,  String feedback);
    void sendSpecifications(String aCommandUid);
    void sendDebug(char* aMesg);
    void display_message(char * aMessage);
public:
    Device(String aName, String aMacAddress, Wiring * wiring);
    Topics topics;
    void setCommunicationLink(Link * aLink);
    void sendHello();
    //void setCommunicationMedium(Communication_medium aCommunication_medium);
    //void setCommunicationMedium(Communication_medium aCommunication_medium, WiFiClient network, char * mqtt_host, int mqtt_port);
    void registerModule(Module * aModule);
    void publish(char * aChannel, char * aMessage);
    void enqueue(Message aMessage);
    void loop();
    void incomingMessage(char* topic, char* payload);
    void led_notification(int level);
};
// =====================================================================================================================
#endif
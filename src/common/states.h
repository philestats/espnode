#ifndef STATES_H
#define STATES_H

#include <Arduino.h>
#include <Vector.h>


class Module;
// =====================================================================================================================
class State{
/* 
*/
    private:
        String key;
        String desc;
        String value;
        Module * owner;
    public:
        State(Module * aModule, String aCommand_name, String aCommand_desc);
        void print();
        String getKey();
        String getDesc();
        void setValue(String aValue);
        String getValue(String aValue);
};
// =====================================================================================================================
#endif
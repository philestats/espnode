// ---------------------------------------------------------------------------------------------------------------------
// References
// ----------
// https://arduinojson.org/v5/faq/
// http://samclane.github.io/Pycharm-Arduino/
// https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-reference/system/system.html
// ---------------------------------------------------------------------------------------------------------------------
#define MQTT_MAX_PACKET_SIZE 2000
#define MQTT_NOT_RETAINED       false
#define MQTT_RETAINED           true

#ifdef ESP32
    #include <FS.h>
    #include "SPIFFS.h"
    #include <esp_wifi.h>
    #include <WiFi.h>
    #include <WiFiClient.h>
    #define ESP_getChipId()     ((uint32_t)ESP.getEfuseMac())
    #define LED_BUILTIN         2
    #define LED_ON              HIGH
    #define LED_OFF             LOW
    #define PUSH_BUTTON         13
    #define HW_TYPE             "ESP32"
    #include <ESPmDNS.h>
#else
    #include <FS.h>                   //this needs to be first, or it all crashes and burns...
    #include <ESP8266WiFi.h>          //https://github.com/esp8266/Arduino
    #include <DNSServer.h>
    #include <ESP8266WebServer.h>
    #define ESP_getChipId()     (ESP.getChipId())
    #define LED_ON              LOW
    #define LED_OFF             HIGH
    #define HW_TYPE             "ESP8266"
#endif
// Pin D2 mapped to pin GPIO2/ADC12 of ESP32, or GPIO2/TXD1 of NodeMCU control on-board LED
#define PIN_LED                 LED_BUILTIN
#define LED_STATUS              LED_BUILTIN

#include <Streaming.h>
#include <ESP_WiFiManager.h>              //https://github.com/khoih-prog/ESP_WiFiManager

// Now support ArduinoJson 6.0.0+ ( tested with v6.14.1 )
#include <ArduinoJson.h>          //https://github.com/bblanchon/ArduinoJson
// ---------------------------------------------------------------------------------------------------------------------
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>
// ---------------------------------------------------------------------------------------------------------------------
#include "src/common/portal.h"
#include "src/common/hermes_protocol.h"
#include "src/common/module.h"
#include "src/common/device.h"
#include "src/common/link.h"
// ---------------------------------------------------------------------------------------------------------------------
#include "src/modules/moduleCumulus.h"
#include "src/modules/modulePorteGarage.h"
#include "src/modules/moduleBME280.h"
#include "src/modules/moduleTIC.h"
// ---------------------------------------------------------------------------------------------------------------------
// general defines
#define CONFIG_FILE             "/ConfigSW.json"
// Pin D2 mapped to pin GPIO2/ADC12 of ESP32, or GPIO2/TXD1 of NodeMCU control on-board LED
#define LED_STATUS              LED_BUILTIN
// ---------------------------------------------------------------------------------------------------------------------
// node working defines
// SLEEP_TIME_SECONDS: the nb of seconds the esp will wait between 2 successive loops in LOOP_MODE_RESET and LOOP_MODE_SLEEP
#define SLEEP_TIME_SECONDS      10
#define SERIAL_MONITOR_SPEED    115200
#define SOFTWARE_NAME           "ESP-NODE"
#define SOFTWARE_VERSION        0.1
// TODO friendlyname en parametre du portail
char device_friendlyname[33] = "Hermes System Device";
int loop_mode = LOOP_INFINITE;
// ---------------------------------------------------------------------------------------------------------------------
// 2 tickers: the 1st one switches on the led with a given period
Ticker tickerAsym;
// the 2nd one switches it off
Ticker tickerAsymOff;
// ---------------------------------------------------------------------------------------------------------------------
static WiFiClient network;
// ---------------------------------------------------------------------------------------------------------------------
// Forward declarations
void toggleLEDAsymOff();
void toggleLEDAsymON();
// =====================================================================================================================
void setup_device_hardware(){
    // TODO à passer dans la classe Module
    // https://circuits4you.com/wp-content/uploads/2018/12/ESP32-Pinout.jpg
    Serial.begin(SERIAL_MONITOR_SPEED);
    pinMode(PUSH_BUTTON, INPUT_PULLDOWN);
    pinMode(LED_STATUS, OUTPUT);
    delay(100);    
}
// ---------------------------------------------------------------------------------------------------------------------
#define _UNUSED_1                   1
#define DEVICE_LED_PIN              2
#define _UNUSED_3                   3
#define _UNUSED_4                   4
#define _UNUSED_5                   5
#define _UNUSED_6                   6
#define _UNUSED_7                   7
#define _UNUSED_8                   8
#define _UNUSED_9                   9
#define _UNUSED_10                  10
#define _UNUSED_11                  11
#define CUMULUS_RELAY_PIN           12
#define _UNUSED_13                  13
#define PORTE_RELAY_PIN             14
#define _UNUSED_15                  15
#define _UNUSED_16                  16
#define _UNUSED_17                  17
#define _UNUSED_18                  18
#define TIC_RX_PIN                  19
#define TIC_TX_PIN                  20
#define BME280_SDA_PIN              21
#define BME280_SCL_PIN              22
#define _UNUSED_23                  23
#define _UNUSED_24                  24
#define PORTE_CLOSE_SENSOR_PIN      25
#define PORTE_OPEN_SENSOR_PIN       26
// ---------------------------------------------------------------------------------------------------------------------
void setup(){
    #define USE_MODULE_CUMULUS
    #define USE_MODULE_BME280
    #define USE_MODULE_PORTE
    #define _USE_MODULE_TIC
    
    setup_device_hardware();
    String mac_address = WiFi.macAddress();
    mac_address.replace(':', '-');
    // -----------------------------------------------------------------------------------------------------------------
    Link * link =  new MqttLink();
    // Link * link = new BluetoothLink();
    link->display();
    // -----------------------------------------------------------------------------------------------------------------
    Wiring wiring_Device[1];
    wiring_Device[0].setPin(DEVICE_LED_PIN).setDesc("LED transmission device").setId(DEVICE_WIRING_LED);
    Device * device = new Device("espnode1", mac_address, wiring_Device);
    device->setCommunicationLink(link);
    // -----------------------------------------------------------------------------------------------------------------
    #ifdef USE_MODULE_CUMULUS
    Wiring wiring_Cumulus[1];
    wiring_Cumulus[0].setPin(CUMULUS_RELAY_PIN).setDesc("Commande relai").setId(CUMULUS_WIRING_RELAY_PIN);
    Module * moduleCumulus = new ModuleCumulus("Cumulus", wiring_Cumulus);
    device->registerModule(moduleCumulus);
    #endif // ----------------------------------------------------------------------------------------------------------
    #ifdef USE_MODULE_BME280
    Wiring wiring_BME280[2];
    wiring_BME280[0].setPin(BME280_SCL_PIN).setDesc("I2C standard clock").setId(BME280_WIRING_SCL_PIN);
    wiring_BME280[1].setPin(BME280_SDA_PIN).setDesc("I2C standard signal").setId(BME280_WIRING_SDA_PIN);
    Module * moduleBME280 = new ModuleBME280("BME280", wiring_BME280);
    device->registerModule(moduleBME280);
    #endif 
    // -----------------------------------------------------------------------------------------------------------------
    #ifdef USE_MODULE_TIC
    Wiring wiring_TIC[2];
    wiring_TIC[0].setPin(TIC_RX_PIN).setDesc("TIC Software Serial RX").setId(TIC_WIRING_RX_PIN);
    wiring_TIC[1].setPin(TIC_TX_PIN).setDesc("TIC Software Serial TX").setId(TIC_WIRING_TX_PIN);
    Module * moduleTIC = new ModuleTIC("TIC", wiring_TIC);
    device->registerModule(moduleTIC);
    #endif 
    // ----------------------------------------------------------------------------------------------------------
    #ifdef USE_MODULE_PORTE
    Wiring wiring_porteG[3];
    wiring_porteG[0].setPin(PORTE_RELAY_PIN).setDesc("Commande relai").setId(PORTE_WIRING_RELAY_PIN);
    wiring_porteG[1].setPin(PORTE_OPEN_SENSOR_PIN).setDesc("Détecteur ouverture").setId(PORTE_WIRING_OPEN_SENSOR_PIN);
    wiring_porteG[2].setPin(PORTE_CLOSE_SENSOR_PIN).setDesc("Détecteur fermeture").setId(PORTE_WIRING_CLOSE_SENSOR_PIN);
    Module * modulePorteG = new ModulePorteGarage("PorteG", wiring_porteG);
    device->registerModule(modulePorteG);
    #endif 
    // ----------------------------------------------------------------------------------------------------------
    // Module * modulePorteD = new ModulePorteGarage("Porte Droite");
    // device->registerModule(modulePorteD);

    device->loop();
}
// ---------------------------------------------------------------------------------------------------------------------
void loop(){
    if (loop_mode == LOOP_MODE_RESET){
        Serial << "LOOP_MODE_RESET: esp will reset in "<< SLEEP_TIME_SECONDS << " seconds";
        delay(SLEEP_TIME_SECONDS * 1000);
        ESP_reboot();
        // /!\ l'esp voit le push bouton pressé au redémarrage
    }
    else if (loop_mode == LOOP_MODE_SLEEP){
        Serial << "LOOP_MODE_SLEEP: esp go to sleep for "<< SLEEP_TIME_SECONDS << " seconds";
        ESP.deepSleep(SLEEP_TIME_SECONDS * 1000 * 1000 );
    }else if (loop_mode == LOOP_INFINITE){
    // bin rien...
    }
}
// ---------------------------------------------------------------------------------------------------------------------
void toggleLEDAsymOff(){
    digitalWrite(LED_STATUS, LED_OFF);
    tickerAsymOff.detach();
}
// ---------------------------------------------------------------------------------------------------------------------
void toggleLEDAsymON(){
    digitalWrite(LED_STATUS, LED_ON);
    tickerAsymOff.attach(0.05, toggleLEDAsymOff);
}
// ---------------------------------------------------------------------------------------------------------------------
bool getUid(char * uid, char jsonPayload[30]){
    Serial << "[getUid] " << jsonPayload <<endl;
    StaticJsonDocument<200> doc;
    char jsonPayload2[50] = "{\"uid\"=\"9487729\", \"arg\": \"off\"}";

    DeserializationError error = deserializeJson(doc, jsonPayload2);

    // Test if parsing succeeds.
    if (error) {
        Serial.print(F("deserializeJson() failed: "));
        Serial.println(error.c_str());
        return false;
    }
    sprintf(uid, doc["uid"]);
    return true;
}
// ---------------------------------------------------------------------------------------------------------------------

/*
mosquitto_pub -h hermes.local -t devices/command/D8-A0-1D-4C-6C-88/device/specifications -m '{"uid":"9487729"}
devices/response/D8-A0-1D-4C-6C-88
{
  "uid": "9487729",
  "modules": [
    {
      "modulename": "Cumulus",
      "commands": [
        {
          "name": "control",
          "desc": "Commande de contrôle du cumulus",
          "arguments": [
            {
              "key": "status",
              "desc": "Etat du cumulus"
            },
            {
              "key": "on",
              "desc": "Allumer le cumulus"
            },
            {
              "key": "off",
              "desc": "Eteindre le cumulus"
            }
          ]
        }
      ],
      "telemetries": [
        {
          "name": "TP",
          "desc": "Temperature + pression capteur BMP280",
          "measures": [
            {
              "key": "T",
              "desc": "Temperature"
            },
            {
              "key": "P",
              "desc": "Pressure"
            }
          ]
        },
        {
          "name": "H",
          "desc": "Humidité",
          "measures": [
            {
              "key": "H",
              "desc": "Humidité"
            },
            {
              "key": "C",
              "desc": "Compteur"
            }
          ]
        }
      ],
      "states": [
        {
          "name": "cum",
          "desc": "Cumulus en marche"
        }
      ]
    },
    {
      "modulename": "PorteGarage",
      "commands": [
        {
          "name": "control_porte1",
          "desc": "Commande de contrôle de la porte 1",
          "arguments": [
            {
              "key": "status",
              "desc": "Etat de la porte 1"
            },
            {
              "key": "open",
              "desc": "Ouvrir la porte 1"
            },
            {
              "key": "close",
              "desc": "Fermer la porte 1"
            },
            {
              "key": "status",
              "desc": "Etat de la porte 2"
            },
            {
              "key": "open",
              "desc": "Ouvrir la porte 2"
            }
          ]
        },
        {
          "name": "control_porte2",
          "desc": "Commande de contrôle de la porte 2"
        }
      ],
      "telemetries": [],
      "states": [
        {
          "name": "pos1",
          "desc": "Position Porte1"
        },
        {
          "name": "pos2",
          "desc": "Position Porte2"
        }
      ]
    }
  ]
}
*/

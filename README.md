# README #

(Documentation to be completed)

espnode is an initiative to easily implement sensors and actuators using espressif esp32 and arduino IDE.
An espnode instance communicates through a standardized mqtt idiom, it can receive orders and send telemetries measures.

### Communication principles ###

The espnode uses some mqtt topic with dedicated usages and fjson formatted messages

  * [PUB] devices/hello
  
    - direction: device -> broker
    - used to announce that the device wakes up
    - message format:
    
       ```json
       {"id":"30-AE-A4-9C-5A-A4","name":"espnode1","sw":"esp-node","ver":"0.1.0","hw":"ESP32","communication":0,"specs":"1"}
       ```
       where:
       
         - id is the macadress of the wifi interface of the device
         - name is a friendly name 
         - sw is the embedded software name 
         - ver is the software version
         - hw is the hardware platform
         - communication is the link type from this enum { WIFI_MQTT = 1, LORA = 2, BLUETOOTH = 3, BLE = 4 };
         - spec = 1 indicates that the device is able to send a detailed description of itself including its modules with commands and telemetries
 
 
 
  * [SUB] devices/command/device-id/module-name/command-name>
  
    - direction broker -> device
    - used to send commands to the device
    - message format:
    
       ```json
       {"uid": "63bf8676-6d30-40cd-b1d2-462237f276ba",  "order": "OPEN" }
       ```
       where:
      
         - uid identifies the command
         - order is the agument key
         - open is the argument value
           The commands and their arguments are setup in each module in the method setup_commands()
  
  
  * [PUB] devices/feedback/device-id
  
    - direction: device -> broker
    - used to publish the state of an incoming command
    - message format:

       ```json
       {"uid":"6081d373-5d3e-429f-bca7-6f231354eb9b","feedback":"CMD_RECEIVED"}
       {"uid":"6081d373-5d3e-429f-bca7-6f231354eb9b","feedback":"CMD_RUNNING"}
       ```
  
  
  * [PUB] devices/response/device-id
   
    - used to publish results of an incoming command
    - TBC
  
  
  * [PUB] devices/telemetry/device-id
  
    - direction: device -> broker
    - used to publish the measurements of a telemetry
    - message format:
       ```json
       {"T":25.56,"H":46.30273,"P":980.9654}
       ```

### Classes ###

Most of the code is in c++ classes:

* Device

  The device class is the representation of the physical device

* Link

  The link is the way the device uses to communicates
  actually bluetooth and mqtt/wifi are implemented, each one inherits from the Link class
  Both provide sending and receiving facilities
  The link instance is owned by the device object
  
* Module 

  A module is a specific implementation of a feature, including sensing and sending telemetries, and reacting to commands
  
* ModuleList  

  ModuleList holds the modules instances. It's the main interface between the device and modules. The moduleList object is unique and is owned by the device
  
* Wiring

  The wiring is used to describe and dispatch the pinout between the different modules, one instance of wiring is owned by each of the modules. 
  The wiring struct is passed to the module constructor
  
* Command

  The command class is the structure holding a command sent to a device. It must respect a syntax describe lower

* Setup

  All setup is done in the .ino program
  First choose the link class to use and create it as an instance of Link:
  
```
      Link * link = new BluetoothLink();`
```

  
  then create the wiring of the device and the device instance:
  
  
```
      wiring_Device[0].setPin(DEVICE_LED_PIN).setDesc("LED transmission device").setId(DEVICE_WIRING_LED);
      Device * device = new Device("espnode1", mac_address, wiring_Device);
```
  
  Finally bind the link to the device:
```
      device->setCommunicationLink(link);
```
  
  Instanciate and register the modules:
  
```
      Wiring wiring_BME280[2];
      wiring_BME280[0].setPin(BME280_SCL_PIN).setDesc("I2C standard clock").setId(BME280_WIRING_SCL_PIN);
      wiring_BME280[1].setPin(BME280_SDA_PIN).setDesc("I2C standard signal").setId(BME280_WIRING_SDA_PIN);
      Module * moduleBME280 = new ModuleBME280("BME280", wiring_BME280);
      device->registerModule(moduleBME280);
```

### Developping a module ###
 a Module is a class inheriting from the abstract base class Module. It must implements the following methods:
    
	* setup_hardware:
	   Use this method to setup the pin wiring related to the device itself (communication link led, etc.)
    * virtual void setup_commands();
    
    * Setup the commands 
```c++    
void ModuleCumulus::setup_commands(){
    Serial << "[ModuleCumulus] [setup_commands]" << endl;
    Command *command1 = new Command(this, "control", "Commande de contr�le du cumulus");
    Command_arg order_ON;
    order_ON.setKey("order").setValue("ON").setDesc("Allumer le cumulus");
    Command_arg order_OFF;
    order_OFF.setKey("order").setValue("OFF").setDesc("Eteindre le cumulus");
    Command_arg order_STATE;
    order_STATE.setKey("order").setValue("STATE").setDesc("Etat du cumulus");
    command1->add_argument(order_ON)->add_argument(order_OFF)->add_argument(order_STATE);
    command1->execute = &ModuleCumulus::process_command;
    this->commands.push_back(command1);
}
```

    
    * virtual void setup_telemetry();
    * virtual void setup_hardware();
    * virtual void setup_states();


### Set up for compiling espnode ###

* Dependencies

	  These librairies are used:

	  - Streaming // https://github.com/janelia-arduino/Streaming

	  - ESP_WiFiManager // https://github.com/khoih-prog/ESP_WiFiManager

	  - ArduinoJson // https://arduinojson.org/

	  - PubSubClient // https://pubsubclient.knolleary.net/
  
      - Vector // https://github.com/janelia-arduino/Vector/



